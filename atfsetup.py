"""atf setup
"""
import re
import os.path
from atfglobals import GlobalObj
import parser
import atfutils

def _setup_loggers():
    """
    """
    return_status = 1
    GlobalObj.initLoggerObj()
    atfdir = GlobalObj.atfdir
    summarylog_file = GlobalObj.summarylog_file
    if GlobalObj.attach_timestamp is 'yes':
        summarylog_file = atfutils.attach_timestamp(GlobalObj.summarylog_file)
    summarylog_level = GlobalObj.summarylog_level
    stdout_dolog = GlobalObj.stdout_dolog
    stdoutlog_level = GlobalObj.stdoutlog_level
    logger = GlobalObj.getLoggerObj()

    summarylog_abspath = os.path.join(atfdir, summarylog_file)
    if logger.addSummarylogHandler(summarylog_abspath, summarylog_level):
        print "IOError: %s" % "Error in Creating SummaryLog"
        return return_status
    if re.match("yes", stdout_dolog, re.IGNORECASE):
        logger.addStdoutlogHandler(stdoutlog_level)

    return 0

def _setup_testrun_info():
    """
    """
    testrun_info_file = GlobalObj.testruninfo_file
    return_status = parser.parse_testrun_info_file(testrun_info_file)
    return return_status

def setup():
    """
    # Setup Summary/Detail/Stdout LogHandlers
    # Parser TestRunInfo file
    #
    """
    return_status = _setup_loggers()
    if return_status:
        return return_status

    return_status = _setup_testrun_info()
    if return_status:
        return return_status

    return 0

__all__ = ['setup']

