"""atfexecute
"""
import os.path
import sys
import imp
from atfglobals import GlobalObj
import pdb
import atfutils


def _execute_testunit(testunitname):
    """
    *) Parse the testcaseslist file in the 'testunit' and select test cases
        specified for testing 'glusterversion'
    *) Call Main.py of 'testunit' to execute the testcases.
    """
    return_status = 1
    logger = GlobalObj.getLoggerObj()
    detaillog_file = GlobalObj.detaillog_file
    detaillog_level = GlobalObj.detaillog_level
    atfdir = GlobalObj.atfdir
    testunits_maindir = GlobalObj.testunits_maindir
    testunit_mainmodule = GlobalObj.testunit_mainmodule
    testunit_abspath = os.path.join(atfdir, testunits_maindir, testunitname)


    _file, path, description = imp.find_module(testunit_mainmodule,
                                                  [testunit_abspath])

    if _file is None:
        logger.error("TestUnit: %s not found" % testunit_abspath)
        return 0

    sys.path.append(testunit_abspath)
    try:
        module = imp.load_module(testunit_mainmodule, _file, path, description)

    except ImportError:
        logger.error("Unable to load '%s' in testunit %s" %
                     (testunit_mainmodule, testunitname))
        logger.error("%s execution Failed" % testunitname)
        return_status = 0

    else:
        detaillog_abspath = os.path.join(testunit_abspath, detaillog_file)
        if GlobalObj.attach_timestamp is 'yes':
            detaillog_abspath = atfutils.attach_timestamp(detaillog_abspath)
        if logger.addDetaillogHandler(detaillog_abspath, detaillog_level):
            logger.error("Unbale to add Detaillog Handler for testunit: %s" %
                         testunitname)
        return_status = module.main()
        logger.removelogHandler('detaillog')

    finally:
        _file.close()
        sys.path.remove(testunit_abspath)
        return return_status

def execute():
    """
    *) Execute the TestsCases form TestUnits specified in TestRunInfo File
    """
    testruninfo_obj = GlobalObj.getTestrunInfoObj()
    testunits = testruninfo_obj.getTestUnits()
    for testunit in testunits:
        _execute_testunit(testunit)

    return 0

__all__ = ['execute']
