"""atfinit module
"""
import os
from atfglobals import GlobalObj

def initialize(args):
    """
    *) Initialize TestrunInfo File
    """
    atfdir = os.path.abspath(args.atfdir)
    if not (os.path.exists(atfdir) and os.path.isdir(atfdir)):
        print "ATFDIR '%s' doesn't exist" % atfdir
        return 1

    testruninfo_abspath = os.path.abspath(args.testruninfo_file)
    if not (os.path.exists(testruninfo_abspath) and
            os.path.isfile(testruninfo_abspath)):
        testruninfo_abspath = os.path.join(atfdir, args.testruninfo_file)
        if not (os.path.exists(testruninfo_abspath) and
                os.patn.isfile(testruninfo_abspath)):
            print "TestrunInfoFile '%s' doesn't exist" % args.testruninfo_file
            return 1

    GlobalObj.testrunid = os.getpid()
    GlobalObj.atfdir = atfdir
    GlobalObj.testruninfo_file = testruninfo_abspath
    GlobalObj.detaillog_file = args.detaillog_file
    GlobalObj.detaillog_level = args.detaillog_level
    GlobalObj.stdout_dolog = args.stdout_dolog
    GlobalObj.stdoutlog_level = args.stdoutlog_level
    GlobalObj.summarylog_file = args.summarylog_file
    GlobalObj.summarylog_level = args.summarylog_level
    GlobalObj.attach_timestamp = args.timestamp
    return 0

__all__ = ['initialize']
