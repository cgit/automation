"""testunit.py is the main module for the testunit.

This module "main" function is called from atfexecute to execute the testunit.
"""
from atfglobals import GlobalObj
import os
import parser
import atfutils
import managerutils
import testcases

reload(testcases)

filename = os.path.abspath(__file__)
dir_path = os.path.dirname(filename)

def initialize():
    """
    * Parse the test environment configuration file
    * SSH to all Servers/Clients specified in testenv file
    """
    logger = GlobalObj.getLoggerObj()
    return_status = 1
    testenv_file = GlobalObj.testenv_file
    testenv_abspath =  os.path.join(dir_path, testenv_file)

    if not (os.path.isfile(testenv_abspath)):
        logger.error("%s not found in %s" % (testenv_file, dir_path))

    return_status = parser.parse_testenv_configfile(testenv_abspath)
    if return_status is not 0:
        return return_status

    return_status = managerutils.ssh_connect_allhosts()
    if return_status is not 0:
        return return_status

    return 0

def setup():
    """
    * Set Active Volume
    """
    return_status = atfutils.set_active_volume("volume1")
    if return_status is not 0:
        return return_status

    return 0

def execute():
    """
    """
    logger = GlobalObj.getLoggerObj()
    return_status = 1
    testcaseslist_file = GlobalObj.testcaseslist_file
    testcaseslist_abspath = os.path.join(dir_path, testcaseslist_file)

    if not (os.path.isfile(testcaseslist_abspath)):
        logger.error("%s not found in %s" % (testcaseslist_file, dir_path))
        return return_status

    else:
        testcaseslist = []
        testcaseslist = parser.parse_testcaseslist_file(testcaseslist_abspath)
        if not testcaseslist:
            logger.error("Skipping TestUnit %s. No testcases to execute"
                         % dir_path)
            return 0
        else:
            passedtestcases = 0
            failedtestcases = 0
            selectedtestcases = len(testcaseslist)

            logger.info("Starting TestUnit: '%s' test execution" % dir_path)

            return_status = testcases.reset_testenv()
            if return_status is not 0:
                return return_status

            return_status = testcases.setup_testenv()
            if return_status is not 0:
                return return_status

            for testcase in testcaseslist:
                function_obj = getattr(testcases, testcase)
                if function_obj:
                    logger.debug("Starting Test: ' %s '" % testcase)

                    return_status = function_obj()
                    if return_status:
                        logger.debug("TestCase '%s' Failed" % testcase)
                        failedtestcases += 1
                    else:
                        logger.debug("TestCase '%s' Passed" % testcase)
                        passedtestcases += 1

                    logger.debug("Ending Test: '%s'" % testcase)

                else:
                    logger.info("TestCase %s not defined in 'testcases' module"
                                % testcase)
                    continue

            logger.info("Selected %d : Passed %d, Failed %d"
                        % (selectedtestcases,
                           passedtestcases,
                           failedtestcases))
            logger.info("Ending TestUnit: '%s' test execution" % dir_path)

    return 0

def cleanup():
    """
    """
    pass

def main():
    """
    """
    return_status = initialize()
    if return_status is not 0:
        return return_status

    return_status = setup()
    if return_status is not 0:
        return return_status

    return_status = execute()
    if return_status is not 0:
        return return_status

    return_status = cleanup()
    if return_status is not 0:
        return return_status

    return 0
