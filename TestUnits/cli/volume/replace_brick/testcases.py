"""testcases for cli/volume/replace_brick
"""
import os
import sys
import time
import parser
import afrutils
import atfutils
import clientutils
import glusterutils
import hostutils
import managerutils
import rbutils
import serverutils
import validate
from atfglobals import GlobalObj
from atfutils import commands

filename = os.path.abspath(__file__)
dir_path = os.path.dirname(filename)
urandom = "/dev/urandom"

def initialize():
    """
    * Parse the test environment configuration file
    * SSH to all Servers/Clients specified in testenv file
    """
    logger = GlobalObj.getLoggerObj()
    testenv_file = GlobalObj.testenv_file
    testenv_abspath =  os.path.join(dir_path, testenv_file)

    if not (os.path.isfile(testenv_abspath)):
        logger.error("%s not found in %s" % (testenv_file, dir_path))

    return_status = parser.parse_testenv_configfile(testenv_abspath)
    if return_status is not 0:
        return return_status

    return_status = managerutils.ssh_connect_allhosts()
    if return_status is not 0:
        return return_status

    return 0

def setup():
    """
    * Set Active Volume
    """
    return_status = atfutils.set_active_volume("volume1")
    if return_status is not 0:
        return return_status

    return 0

def reset_testenv():
    output = clientutils.umountall()
    assert_success_status = atfutils.assert_success(output["exitstatus"])
    if assert_success_status is not 0:
        return assert_success_status

    glusterutils.volume_stop("server1", force=True)
    glusterutils.volume_delete("server1")
    glusterutils.glusterd_stop_allservers()
    glusterutils.glusterd_remove_dir_allservers()
    glusterutils.glusterd_remove_logs_allservers()
    return 0

def setup_testenv():
    """
    """
    output = glusterutils.glusterd_start_allservers(force=True)
    assert_success_status = atfutils.assert_success(output["exitstatus"])
    if assert_success_status is not 0:
        return assert_success_status

    output = glusterutils.peer_probe("server1")
    assert_success_status = atfutils.assert_success(output["exitstatus"])
    if assert_success_status is not 0:
        return assert_success_status

    output = glusterutils.create_brick_allservers()
    assert_success_status = atfutils.assert_success(output["exitstatus"])
    if assert_success_status is not 0:
        return assert_success_status

    output = glusterutils.volume_create("server1")
    assert_success_status = atfutils.assert_success(output["exitstatus"])
    if assert_success_status is not 0:
        return assert_success_status

    output = glusterutils.volume_set("server1",
                                     key="diagnostics.client-log-level",
                                     value="DEBUG")

    output = glusterutils.volume_start("server1")
    assert_success_status = atfutils.assert_success(output["exitstatus"])
    if assert_success_status is not 0:
        return assert_success_status

    return 0

def bug2909():
    """
    Note: for replace-brick , we need the fuse module on the servers.
    The replace-brick 'start' operation shows successful.
    But the replace-brick 'status'  operation fails.
    """
    return_status = initialize()
    if return_status is not 0:
        return return_status

    return_status = setup()
    if return_status is not 0:
        return return_status

    return_status = reset_testenv()
    if return_status is not 0:
        return return_status

    return_status = setup_testenv()
    if return_status is not 0:
        return return_status

    output = glusterutils.volume_replacebrick("server1", "brick1", "brick3",
                                              "start")
    expected_message = "started successfully"
    validation_status = atfutils.validate_output(output, 0, expected_message)
    if validation_status is not 0:
        return validation_status

    output = glusterutils.volume_replacebrick("server1", "brick1", "brick3",
                                              "status")
    expected_message = "Number of files migrated = 0        Migration complete"
    validation_status = atfutils.validate_output(output, 0, expected_message)
    if validation_status is not 0:
        return validation_status

    output = glusterutils.volume_replacebrick("server1", "brick1", "brick3",
                                              "commit")
    expected_message = "commit successful"
    validation_status = atfutils.validate_output(output, 0, expected_message)
    if validation_status is not 0:
        return validation_status

    return 0

def bug3033():
    """
    Changes to replace-brick and syntask interface
    """
    return_status = initialize()
    if return_status is not 0:
        return return_status

    return_status = setup()
    if return_status is not 0:
        return return_status

    return_status = reset_testenv()
    if return_status is not 0:
        return return_status

    return_status = setup_testenv()
    if return_status is not 0:
        return return_status

    output = clientutils.mount("mount1")
    assert_success_status = atfutils.assert_success(output["exitstatus"])
    if assert_success_status is not 0:
        return assert_success_status

    command = "mkdir -p {1..50}"
    output = clientutils.execute_on_mount("mount1", command)
    assert_success_status = atfutils.assert_success(output["exitstatus"])
    if assert_success_status is not 0:
        return assert_success_status

    output = glusterutils.volume_replacebrick("server1", "brick1", "brick3",
                                              "start")
    expected_message = "started successfully"
    validation_status = atfutils.validate_output(output, 0, expected_message)
    if validation_status is not 0:
        return validation_status

    timeout = 30
    rb_completion_status = rbutils.wait_till_rb_completes(timeout, "server1",
                                                          "brick1", "brick3")
    if rb_completion_status is not 0:
        return rb_completion_status

    output = glusterutils.volume_replacebrick("server1", "brick1", "brick3",
                                              "commit")
    expected_message = "commit successful"
    validation_status = atfutils.validate_output(output, 0, expected_message)
    if validation_status is not 0:
        return validation_status

    bricks = ['brick2', 'brick3']
    commands = ['getfattr -n trusted.glusterfs.pump-source-complete ',
                'getfattr -n trusted.glusterfs.pump-sink-complete ',
                'getfattr -n trusted.glusterfs.pump-path ']
    for brick in bricks:
        for command in commands:
            command = command + "<" + brick + ".path>"
            output = serverutils.execute_on_brick(brick, command)
            expected_output = "No such attribute"
            validation_status = atfutils.validate_output(output, 0,
                                                         expected_output,
                                                         stream="stderr")
            if validation_status is not 0:
                return validation_status

    return 0

def bug3036():
    """
    self-heal problem in replace-brick
    """
    input_file = urandom
    file1 = "f1"
    mounts = ["mount1"]
    bricks = ["brick1", "brick2"]
    new_bricks = ["brick2", "brick3"]

    return_status = initialize()
    if return_status is not 0:
        return return_status

    return_status = setup()
    if return_status is not 0:
        return return_status

    return_status = reset_testenv()
    if return_status is not 0:
        return return_status

    return_status = setup_testenv()
    if return_status is not 0:
        return return_status

    output = clientutils.mount("mount1")
    assert_success_status = atfutils.assert_success(output["exitstatus"])
    if assert_success_status is not 0:
        return assert_success_status

    output_file = file1
    command = ' '.join([commands['dd'], "if="+input_file, "of="+output_file,
                        "bs=2M", "count=1024"])
    output = clientutils.execute_on_mount("mount1", command)
    expected_output = "2147483648"
    validation_status = atfutils.validate_output(output, 0, expected_output,
                                                 stream="stderr")
    if validation_status is not 0:
        return validation_status

    command = ' '.join([commands['ls'], "-l", output_file])
    output = clientutils.execute_on_mount("mount1", command)
    expected_output = "2147483648"
    validation_status = atfutils.validate_output(output, 0, expected_output)
    if validation_status is not 0:
        return validation_status
    return_status = validate.validate_on_bricks(bricks, command, 0,
                                                expected_output)
    if return_status is not 0:
        return return_status

    output = glusterutils.volume_replacebrick("server1", "brick1", "brick3",
                                              "start")
    expected_message = "started successfully"
    validation_status = atfutils.validate_output(output, 0, expected_message)
    if validation_status is not 0:
        return validation_status

    timeout = 60
    rb_completion_status = rbutils.wait_till_rb_completes(timeout, "server1",
                                                          "brick1", "brick3")
    if rb_completion_status is not 0:
        return rb_completion_status

    output = glusterutils.volume_replacebrick("server1", "brick1", "brick3",
                                              "commit")
    expected_message = "commit successful"
    validation_status = atfutils.validate_output(output, 0, expected_message)
    if validation_status is not 0:
        return validation_status

    validation_status = validate.validate_md5sums(mounts, new_bricks)
    if validation_status is not 0:
        return validation_status

    return 0
