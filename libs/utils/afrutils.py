import time
import atfutils
import glusterutils

def wait_till_selfheal_completes(timeout):
    """
    Wait for selfheal to Complete
    """
    time.sleep(timeout)
    selfheal_completion_status = 0
    return selfheal_completion_status

def wait_till_brick_reboot(timeout):
    """
    Wait for brick to reboot
    """
    time.sleep(timeout)
    reboot_completion_status = 0
    return reboot_completion_status

def set_read_subvolume(from_server, subvolumeid):
    """
    Parameters:
        from_server: volume set read-subvolume will be executed on 'from_server'
        subvolumeid (Ex: client-0)
    """
    output = atfutils.get_new_output_obj()
    active_volume = atfutils.get_active_volume()
    if active_volume is None:
        output["exitstatus"] = 1
        return output

    volumename = active_volume.volumename
    subvolume = "-".join([volumename, subvolumeid])
    output = glusterutils.volume_set(from_server,
                                     key="cluster.read-subvolume",
                                     value=subvolume)
    return output

def disable_self_heal(from_server, *types):
    """
    Parameters:
        from_server: volume self-heal off will be executed on 'from_server'
        *type : type of self heal (it can be data, entry, metadata)
    Usage:
        disable_self_heal("server1", data)
        disable_self_heal("server1", data, entry)
        disable_self_heal("server1", data, metadata)
        disable_self_heal("server1", entry, metadata)
        disable_self_heal("server1", data, entry, metadata)
    """
    output = atfutils.get_new_output_obj()
    selfheal = {
        'data'  :   'cluster.data-self-heal',
        'entry' :   'cluster.entry-self-heal',
        'metadata'  :   'cluster.metadata-self-heal'
        }

    for _type in types:
        if selfheal.has_key(_type):
            output = glusterutils.volume_set(from_server, key=selfheal[_type],
                                             value="off")
            assert_success_status = atfutils.assert_success(output["exitstatus"])
            if assert_success_status is not 0:
                return output

    if output['exitstatus'] is None:
        output['exitstatus'] = 1
        output['stderrdata'] = "Unable to Disable self-heal %s" % str(types)
    else:
        output['exitstatus'] = 0
        output['stdoutdata'] = "Successfully Disabled self-healed %s" % str(types)

    return output

def setup_testenv_gfid_heal():
    """
    """
    output = glusterutils.glusterd_start_allservers(force=True)
    assert_success_status = atfutils.assert_success(output["exitstatus"])
    if assert_success_status is not 0:
        return assert_success_status

    output = glusterutils.create_brick_allservers()
    assert_success_status = atfutils.assert_success(output["exitstatus"])
    if assert_success_status is not 0:
        return assert_success_status

    return 0

def create_volume_gfid_heal(from_server):
    """
    """
    output = glusterutils.peer_probe(from_server)
    assert_success_status = atfutils.assert_success(output["exitstatus"])
    if assert_success_status is not 0:
        return assert_success_status

    output = glusterutils.volume_create(from_server)
    assert_success_status = atfutils.assert_success(output["exitstatus"])
    if assert_success_status is not 0:
        return assert_success_status

    output = glusterutils.volume_set(from_server, key="self-heal-daemon",
                                     value="off")
    assert_success_status = atfutils.assert_success(output["exitstatus"])
    if assert_success_status is not 0:
        return assert_success_status

    glusterutils.volume_set(from_server, key="diagnostics.client-log-level",
                            value="DEBUG")

    glusterutils.volume_set(from_server, key="diagnostics.brick-log-level",
                            value="DEBUG")
    return 0
