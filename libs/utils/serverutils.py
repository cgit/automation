"""serverutils module
"""
import re
import atfutils
import hostutils
from atfglobals import GlobalObj

def md5sum_of_brick(brickkey):
    """
    Parameter: brick (tye: string)
    Returns: output of arequal-checksum command execution(type:dict)
        Key : Value of the Output
        exitstatus: exit status of the arequal-checksum command on brick
        stdoutdata: stdout data of arequal-checksum command execution
        stderrdata: stderr data of arequal-checksum command execution
    """
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()
    env = GlobalObj.getTestenvObj()

    raw_brick_obj = env.getRawBrick(brickkey)
    if not raw_brick_obj:
        logger.error("InValid Brick. %s not defined in TestEnvironment" %
                     brickkey)
        output["exitstatus"] = 1
        return output
    else:
        serverkey = re.split("\.", raw_brick_obj.hostname, maxsplit=1)[0]

    brick_obj = env.getBrick(brickkey)
    if not brick_obj:
        logger.error("InValid Brick. %s not defined in TestEnvironment"
                 % brickkey)
        output["exitstatus"] = 1
        return output
    else:
        brick_path = brick_obj.path

    output = hostutils.md5sum(serverkey, brick_path)
    return output

def md5sum_of_bricks(bricks):
    """
    Description:
        Calculate md5sum of bricks using arequal-checksum

    Parameters: bricks (type: List)

    Returns: md5sums of all the bricks (type: dict)
        Keys: bricks
        Value: ouput (type:dict)
            exitstatus: exit status of the arequal-checksum command on brick
            stdoutdata: stdout data of arequal-checksum command execution
            stderrdata: stderr data of arequal-checksum command execution
    """
    md5sums = {}
    for brickkey in bricks:
        output = md5sum_of_brick(brickkey)
        md5sums[brickkey] = output

    return md5sums

def get_gfid_on_brick(brickkey, filename="."):
    """
    """
    base_command = "getfattr -n 'trusted.gfid' -e hex"
    command = ' '.join([base_command, filename])
    output = execute_on_brick(brickkey, command)
    if output['stdoutdata'] is None or (not output['stdoutdata']):
        output['stdoutdata'] = ""
    else:
        output['stdoutdata'] = str(output['stdoutdata'])

    return output

def get_gfid_on_bricks(bricks, filename="."):
    """
    """
    gfid_on_bricks = {}
    for brickkey in bricks:
        output = get_gfid_on_brick(brickkey, filename)
        gfid_on_bricks[brickkey] = output

    return gfid_on_bricks

def execute_on_brick(brickkey, command, commandInput=None):
    """
    """
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()
    env = GlobalObj.getTestenvObj()

    raw_brick_obj = env.getRawBrick(brickkey)
    if not raw_brick_obj:
        logger.error("InValid Brick. %s not defined in TestEnvironment"
                     % brickkey)
        output["exitstatus"] = 1
        return output
    else:
        serverkey = re.split("\.", raw_brick_obj.hostname, maxsplit=1)[0]

    brick_obj = env.getBrick(brickkey)
    if not brick_obj:
        logger.error("InValid Brick. %s not defined in TestEnvironment"
                     % brickkey)
        output["exitstatus"] = 1
        return output
    else:
        exportdirpath = brick_obj.path

    command = "cd %s ; %s" % (exportdirpath , command)
    output = hostutils.execute_command(serverkey, command, commandInput)
    return output

def execute_on_bricks(bricks, command, commandInput=None):
    """
    Parameters:
        bricks: List of bricks (Ex: [brick1, brick2, brick3, ...])
        command: Command to execute on brick
    """
    all_outputs = {}

    for brickkey in bricks:
        output = execute_on_brick(brickkey, command, commandInput)
        all_outputs[brickkey] = output

    return all_outputs
