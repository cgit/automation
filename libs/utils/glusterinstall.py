"""
Install module contains functions required to install
glusterfs using tar , git or rpm and
supporting functions
"""
import re
from atfglobals import GlobalObj
import atfutils
import os
import hostutils
import pdb

system_dirs = re.compile('(/bin|/boot|/dev|/etc|/lib|/mnt|/net|/root|/sbin|\
/usr|/var|/sys)\/?$')


def stop_gluster_processes(hostkey):
    """
    stopping all the gluster processes
    """
    logger = GlobalObj.getLoggerObj()
    env = GlobalObj.getTestenvObj()
    kill_command = "killall glusterd ; killall glusterfsd ; killall glusterfs"
    output = hostutils.execute_command(hostkey, kill_command)
    return output

def gluster_cleanup(hostkey):
    """
    Cleaning /etc/glusterd, usr/local/sbin and /usr/sbin
    """
    logger = GlobalObj.getLoggerObj()
    remove_command = "rm -rf /etc/glusterd/ && rm -rf /usr/local/sbin/gluster* \
    && rm -rf /usr/sbin/gluster* "
    output = hostutils.execute_command(hostkey, remove_command)
    return output

def source_cleanup(hostkey,version,down_path):
    """
    Cleaning up the directories of the source if
    already present
    """
    chdir = 'cd ' + down_path + ' && '
    logger = GlobalObj.getLoggerObj()
    remove_rep ='rm -rf glusterfs.git && rm -rf glusterfs-' + version + ' && \
    rm -rf glusterfs-' + version + '.tar.gz && rm -rf glusterfs.rpm.' + version
    remove_command = chdir + remove_rep
    output = hostutils.execute_command(hostkey,remove_command)
    return_status = atfutils.assert_success(output['exitstatus'])
    if return_status:
        logger.error("Unable to remove source")
        return output
    return output


def rpm_check(hostkey):
    """
    checking for rpm installation of gluster and removing it
    """
    logger = GlobalObj.getLoggerObj()
    cleanup_command = "rpm -qa | grep gluster | xargs rpm -e"
    output = hostutils.execute_command(hostkey, cleanup_command)
    return output

def configure(hostkey, chdir, version):
    """
    configuring the souce with options simlar to that we use for rpm build
    """
    logger = GlobalObj.getLoggerObj()

    configure = "./autogen.sh && ./configure \
    --build=x86_64-unknown-linux-gnu \
    --host=x86_64-unknown-linux-gnu \
    --target=x86_64-redhat-linux-gnu \
    --program-prefix= \
    --prefix=/opt/glusterfs/"+ version +"\
    --exec-prefix=/opt/glusterfs/" + version + "\
    --bindir=/opt/glusterfs/" + version + "/bin \
    --sbindir=/opt/glusterfs/" + version + "/sbin \
    --sysconfdir=/etc \
    --datadir=/opt/glusterfs/" + version + "/share \
    --includedir=/opt/glusterfs/" + version + "/include \
    --libdir=/opt/glusterfs/" + version + "/lib64 \
    --libexecdir=/usr/local/libexec/ \
    --localstatedir=/var \
    --sharedstatedir=/var/lib \
    --mandir=/usr/share/man \
    --infodir=/usr/share/info"
    configure_command = chdir + configure
    logger.debug('%s: Executing command : %s' %(hostkey, configure_command))
    output = hostutils.execute_command(hostkey, configure_command)
    return output

def make(hostkey, chdir):
    """
    """
    logger = GlobalObj.getLoggerObj()
    make = "make && make install"
    make_command = chdir + make
    output = hostutils.execute_command(hostkey, make_command)
    return output

def symlink(hostkey, install_path):
    """
    creating symlinks /usr/sbin/gluster* to /opt/glusterfs/version/sbin/gluster*
     """
    logger = GlobalObj.getLoggerObj()
    symlink_command = "ln -s " + install_path + "sbin/gluster /usr/sbin/gluster\
    && ln -s " + install_path + "sbin/glusterd /usr/sbin/glusterd && \
    ln -s " + install_path + "sbin/glusterfsd /usr/sbin/glusterfsd && \
    ln -s " + install_path + "sbin/glusterfs /usr/sbin/glusterfs"
    output = hostutils.execute_command(hostkey, symlink_command)
    return output

def download_scp(version,down_path):
    """
    Downloads the tar ball with given version on one machine in the given
    download path.
    Then it does scp to all the other machines involved
    """
    logger = GlobalObj.getLoggerObj()
    env = GlobalObj.getTestenvObj()
    download_url = "http://bits.gluster.com/pub/gluster/glusterfs/src/glusterfs-\
" + version + ".tar.gz"
    hostkeys = env.getHostsKeys()
    main_host = hostkeys[0]
    output = hostutils.mkdir(main_host, down_path)
    output = source_cleanup(main_host, version, down_path)
    #changing directory to download path
    chdir = 'cd ' + down_path + ' && '
    wget_command = 'wget ' + download_url
    download_command = chdir + wget_command
    output = hostutils.execute_command(main_host,download_command)
    return_status = atfutils.assert_success(output['exitstatus'])
    if return_status:
        logger.error("Unable to download the tarball")
        return output

    for hostkey in hostkeys:
        if not hostkey is main_host:
            output = hostutils.mkdir(hostkey, down_path)
            output = source_cleanup(hostkey, version, down_path)
            chdir = 'cd ' + down_path + ' && '
            host_obj = env.getHost(hostkey)
            host_value = 'root@' + host_obj.hostname + ':' + down_path
            scp = 'scp -o StrictHostKeyChecking=no glusterfs-' + version + '\
.tar.gz ' + host_value
            scp_command = chdir + scp
            output = hostutils.execute_command(main_host, scp_command)
            return_status = atfutils.assert_success(output['exitstatus'])
            if return_status:
                logger.error("Unable to scp the tarball")
                return output
    return output

def clone_scp(branch, down_path):
    """
    It clones the glusterfs git repository on one machine in the download
    given . Then it checksout to the given branch.
    It genenrates the tarball from the git and
    """
    output = atfutils.get_new_output_obj()
    logger = GlobalObj.getLoggerObj()
    env = GlobalObj.getTestenvObj()

    git_url = "https://github.com/gluster/glusterfs.git"
    hostkeys = env.getHostsKeys()
    main_host = hostkeys[0]
    output = hostutils.mkdir(main_host, down_path)
    output = source_cleanup(main_host, branch, down_path)
    #changing directory to download path
    chdir = 'cd '+down_path+' && '

    git_command = 'git clone' + ' ' + git_url + ' ' + 'glusterfs.git'
    clone_command = chdir + git_command
    output = hostutils.execute_command(main_host,clone_command)
    return_status = atfutils.assert_success(output['exitstatus'])
    if return_status:
        logger.error("Unable to do git clone")
        return output

    chdir = 'cd ' + down_path + 'glusterfs.git'+' && '

    if branch is "3.2git":
        git_command = 'git checkout release-3.2 && git pull '
    else:
        git_command = 'git pull'
    pull_command = chdir + git_command
    output = hostutils.execute_command(main_host, pull_command)
    return_status = atfutils.assert_success(output['exitstatus'])
    if return_status:
        logger.error("Unable to do git checkout or git pull")
        return output

    tar_command = './autogen.sh && ./configure --enable-fusermount && \
    make dist && cp glusterfs-' + branch + '.tar.gz ../'
    command = chdir + tar_command
    output = hostutils.execute_command(main_host,command)
    return_status = atfutils.assert_success(output['exitstatus'])
    if return_status:
        logger.error("Unable to do git clone")
        return output

    for hostkey in hostkeys:
        if not hostkey is main_host:
            output = hostutils.mkdir(hostkey, down_path)
            output = source_cleanup(hostkey, branch, down_path)
            chdir = 'cd ' + down_path + ' && '
            host_obj = env.getHost(hostkey)
            host_value = 'root@' + host_obj.hostname + ':' + down_path
            scp = 'scp -o StrictHostKeyChecking=no glusterfs-' + branch + '\
.tar.gz ' + host_value
            scp_command = chdir+scp
            output = hostutils.execute_command(main_host,scp_command)
            return_status = atfutils.assert_success(output['exitstatus'])
            if return_status:
                logger.error("Unable to scp the tarball")
                return output
    return output

def gluster_install_tar(version):
    """
    Installls the Glusterfs of given version in all the
    machines defined in the testenv.cfg og testunit.
    ex: gluster_install_tar('3.3.0qa21')
    """
    output = atfutils.get_new_output_obj()
    logger = GlobalObj.getLoggerObj()
    env = GlobalObj.getTestenvObj()
    install_path = "/opt/glusterfs/" + version + "/"

    down_path = ''.join(env.getGlusterDownloadPaths())
    output = atfutils.get_new_output_obj()
    if not down_path.endswith('/'):
        down_path = down_path + '/'

    if system_dirs.match(down_path):
        logger.error("System Directiories cannot be created")
        output['exitstatus'] = 1
        return output

    output = download_scp(version,down_path)
    if output['exitstatus']:
        return output

    host_keys = env.getHostsKeys()
    for hostkey in host_keys:
        output = stop_gluster_processes(hostkey)
        output = rpm_check(hostkey)
        output = gluster_cleanup(hostkey)
        if output['exitstatus']:
            return output

        chdir = 'cd ' + down_path + ' && '

        extract = 'tar -xzf glusterfs-' + version + '.tar.gz'
        extract_command = chdir + extract
        output = hostutils.execute_command(hostkey, extract_command)
        return_status = atfutils.assert_success(output['exitstatus'])
        if return_status:
            logger.error("unable to extract from the tar ball")
            return output
        #changing directory to the glusterfs director

        chdir = 'cd ' + down_path + 'glusterfs-' + version + ' && '
        output = configure(hostkey, chdir, version)
        return_status = atfutils.assert_success(output['exitstatus'])
        if return_status:
            logger.error("unable to build the source")
            return output

        output = make(hostkey, chdir)
        return_status = atfutils.assert_success(output['exitstatus'])
        if return_status:
            logger.error("unable to build the source")
            return output

        output = symlink(hostkey, install_path)
        return_status = atfutils.assert_success(output['exitstatus'])
        if return_status:
            logger.error('unable to create symlinks')
            return output
    output['exitstatus'] = 0
    return output

def gluster_install_git(branch):
    """
    Installs the Glusterfs with given Branch.
    ex:gluster_install_git('3git') or
       gluster_install_git('3.2git')
    """
    output = atfutils.get_new_output_obj()
    logger = GlobalObj.getLoggerObj()
    env = GlobalObj.getTestenvObj()
    install_path = "/opt/glusterfs/" + branch + "/"

    down_path = ''.join(env.getGlusterDownloadPaths())
    output = atfutils.get_new_output_obj()
    if not down_path.endswith('/'):
        down_path = down_path + '/'

    if system_dirs.match(down_path):
        logger.error("System Directiories cannot be created")
        output['exitstatus'] = 1
        return output

    output = clone_scp(branch, down_path)
    if output['exitstatus']:
        return output

    host_keys = env.getHostsKeys()

    for hostkey in host_keys:
        output = stop_gluster_processes(hostkey)

        output = rpm_check(hostkey)

        output = gluster_cleanup(hostkey)
        if output['exitstatus']:
            return output

        chdir = 'cd ' + down_path + ' && '

        extract = 'tar -xzf glusterfs-' + branch + '.tar.gz'
        extract_command = chdir + extract
        output = hostutils.execute_command(hostkey, extract_command)
        return_status = atfutils.assert_success(output['exitstatus'])
        if return_status:
            logger.error("unable to extract from the tar ball")
            return output
        #changing directory to the glusterfs director

        chdir = 'cd ' + down_path + 'glusterfs-' + branch + ' && '
        output = configure(hostkey, chdir, branch)
        return_status = atfutils.assert_success(output['exitstatus'])
        if return_status:
            logger.error("unable to build the source")
            return output

        output = make(hostkey, chdir)
        return_status = atfutils.assert_success(output['exitstatus'])
        if return_status:
            logger.error("unable to build the source")
            return output

        output = symlink(hostkey, install_path)
        return_status = atfutils.assert_success(output['exitstatus'])
        if return_status:
            logger.error('unable to create symlink')
            return output
    output['exitstatus'] = 0
    return output

def rpm_scp(version, down_path, *components):
    """
    This functions downloads all the rpms mentioned in the
    components into one machine , then it scp the directory to all
    other machines involved
    """
    rpm_path = down_path + 'glusterfs.rpm.' + version

    output = atfutils.get_new_output_obj()
    logger = GlobalObj.getLoggerObj()
    env = GlobalObj.getTestenvObj()

    host_keys = env.getHostsKeys()
    main_host = host_keys[0]
    output = hostutils.mkdir(main_host, down_path)
    output = source_cleanup(main_host, version, down_path)
    output = hostutils.mkdir(main_host, rpm_path)
    #changing directory to download path
    chdir = 'cd '+rpm_path+' && '

    component_url = "http://bits.gluster.com/pub/gluster/glusterfs/\
" + version + "/x86_64/glusterfs-" + version + "-1.x86_64.rpm"
    wget_command = "wget " + component_url
    down_command = chdir + wget_command
    output = hostutils.execute_command(main_host, down_command)
    return_status = atfutils.assert_success(output['exitstatus'])
    if return_status:
        logger.error('unable to to download the rpms')
        return output

    for component in components:
        component_url = "http://bits.gluster.com/pub/gluster/glusterfs/\
" + version + "/x86_64/glusterfs-" + component + "-" + version + "-1.x86_64.rpm"
        wget_command = "wget " + component_url
        down_command = chdir + wget_command
        output = hostutils.execute_command(main_host, down_command)
        return_status = atfutils.assert_success(output['exitstatus'])
        if return_status:
            logger.error('unable to to download the rpms')
            return output

    for hostkey in host_keys:
        if not hostkey is main_host:
            output = hostutils.mkdir(hostkey, down_path)
            output = source_cleanup(hostkey, version, down_path)
            output = hostutils.mkdir(hostkey, rpm_path)
            chdir = 'cd ' + down_path + ' && '
            host_obj = env.getHost(hostkey)
            host_value = 'root@' + host_obj.hostname + ':' + down_path
            scp = 'scp -o StrictHostKeyChecking=no -r glusterfs.rpm.\
' + version + ' ' + host_value
            scp_command = chdir+scp
            output = hostutils.execute_command(main_host,scp_command)
            return_status = atfutils.assert_success(output['exitstatus'])
            if return_status:
                logger.error("Unable to scp the rpms")
                return output

    return output

def rpm_install(hostkey, version, down_path, component):
    """
    This function installs the rpms given, one at a time.
    """
    logger = GlobalObj.getLoggerObj()
    rpm_path = down_path + 'glusterfs.rpm.' + version
    if component == '':
        install_command = "rpm -ivh glusterfs-" + version+"\
-1.x86_64.rpm --nodeps"
    else:
        install_command = "rpm -ivh glusterfs-" + component + "-" + version+"\
-1.x86_64.rpm --nodeps"
    chdir ='cd ' + rpm_path + ' && '
    rpm_command = chdir + install_command
    output = hostutils.execute_command(hostkey, rpm_command)
    return_status = atfutils.assert_success(output['exitstatus'])
    if return_status:
        logger.error('unable to install rpm')
        return output

    return output

def gluster_install_rpm(version, *components):
    """
    Installs the Glusterfs with given version and
    the given rpms.
    components can have: fuse, debuginfo, devel, geo-replication
    rdma, server
    ex:gluster_install_rpm('3.3.0qa21','core','fuse') or
       gluster_install_git('3.3.0qa21','fuse','debuginfo,'core')
    """
    logger = GlobalObj.getLoggerObj()
    env = GlobalObj.getTestenvObj()
    install_path = "/opt/glusterfs/" + version + "/"

    down_path = ''.join(env.getGlusterDownloadPaths())
    output = atfutils.get_new_output_obj()
    if not down_path.endswith('/'):
        down_path = down_path + '/'

    if system_dirs.match(down_path):
        logger.error("System Directiories cannot be created")
        output['exitstatus'] = 1
        return output
    rpm_path = down_path + 'glusterfs.rpm.' + version

    output = rpm_scp(version, down_path, *components)

    host_keys = env.getHostsKeys()
    for hostkey in host_keys:

        output = stop_gluster_processes(hostkey)
        output = rpm_check(hostkey)
        output = gluster_cleanup(hostkey)
        if output['exitstatus']:
            return output
        output = rpm_install(hostkey, version, down_path, '')
        if output['exitstatus']:
            return output
        if 'fuse' in components:
            output = rpm_install(hostkey, version, down_path, 'fuse')
            if output['exitstatus']:
                return output
        if 'debuginfo' in components:
            output = rpm_install(hostkey, version, down_path, 'debuginfo')
            if output['exitstatus']:
                return output
        if 'devel' in components:
            output = rpm_install(hostkey, version, down_path,  'devel')
            if output['exitstatus']:
                return output
        if 'rdma' in components:
            output = rpm_install(hostkey, version, down_path, 'rdma')
            if output['exitstatus']:
                return output
        if 'geo-replication' in components:
            output = rpm_install(hostkey, version, down_path, 'geo-replication')
            if output['exitstatus']:
                return output
        if 'server' in components:
            output = rpm_install(hostkey, version, down_path, 'server')
            if output['exitstatus']:
                return output

    output['exitstatus'] = 0
    return output
