"""atfutils module contains  general purpose wrappers

*) assert_success
*) assert_failure
*) print-stdout
*) print_stderr
*) set_active_volume
"""
import re
import time
import inspect
from atfglobals import GlobalObj

commands = {
    'wc'    :   'wc',
    'ls'    :   'ls',
    'stat'  :   'stat',
    'echo'  :   'echo',
    'cat'   :   'cat',
    'touch' :   'touch',
    'rename':   'mv',
    'unlink':   'rm',
    'dd'    :   'dd',
    'chmod' :   'chmod',
    'chown' :   'chown',
    'mkdir' :   'mkdir -p',
    'mkfs'  :   'mkfs',
    'mount' :   'mount',
    'umount'    : 'umount',
    'hardlink'  : 'ln',
    'symlink'   : 'ln -s',
    'setattr'   : 'setfattr',
    'getattr'   : 'getfattr',
    'truncate'  : 'truncate',
    'unlink_force'      : 'rm -f',
    'unlink_dir_force'  : 'rm -rf'
    }

def get_new_output_obj():
    output = {}
    for key in ('exitstatus', 'stdoutdata', 'stderrdata'):
        output[key] = None
    return output

def assert_success(exit_status):
    """
    """
    if exit_status is not 0:
        return 1
    else:
        return 0

def assert_failure(exit_status):
    """
    """
    if exit_status is 0:
        return 1
    else:
        return 0

def assert_success_of_outputs(outputs):
    """
    """
    caller = inspect.stack()[1][3]
    logger = GlobalObj.getLoggerObj()
    assert_success_flag = True
    for key in outputs.keys():
        output = outputs[key]
        assert_success_status = assert_success(output["exitstatus"])
        if assert_success_status is not 0:
            assert_success_flag = False
            logger.error("%s Failed to Execute %s") % (key, caller)

    if assert_success_flag is False:
        return 1
    else:
        return 0

def expect(actual_string, expected_string):
    """
    """
    escaped_pattern = re.escape(expected_string)
    new_pattern = re.compile(escaped_pattern, re.IGNORECASE)

    matched = False
    if not expected_string:
        matched = True
    else:
        if re.search(new_pattern, actual_string):
            matched = True

    return matched

def validate_output(output, expected_exit_status, expected_output,
                    stream="stdout"):
    """
    Parameters:
        output: This is dictionary which is return value of
                executecommand(ssh module)

        expected_exit_status: exit_status we are expecting

        expected_output : output expected from the execution of command

    Returns:
        True: if output we got from the command execution matches with
                expected_exit_status and expected_output is

        False: if output doesn't match with expected_exit_status or
                expected_output
    """
    logger = GlobalObj.getLoggerObj()
    exit_status_match = False
    output_data_match = False

    if None in (output["stdoutdata"], output["stderrdata"]):
        logger.error("Error in Test Environment")
        return 1

    if expected_exit_status is not 0:
        assert_exit_status = assert_failure(output['exitstatus'])
    else:
        assert_exit_status = assert_success(output['exitstatus'])
    if assert_exit_status is 0:
        exit_status_match = True

    if not expected_output:
        output_data_match = True
    else:
        if stream is "stderr":
            actual_output = str(output['stderrdata'])
        else:
            actual_output = str(output['stdoutdata'])
        output_data_match = expect(actual_output, expected_output)

    if exit_status_match is False:
        logger.error("Expected exit status is '%s'.Actual exit status is '%s'" %
                     (expected_exit_status, output['exitstatus']))
    if output_data_match is False:
        logger.error("Expected ouput is '%s'.Actual output is '%s'" %
                     (expected_output, actual_output))

    if (exit_status_match and output_data_match) is True:
        return 0
    else:
        return 1

def print_stdout(stdoutdata):
    """
    """
    logger = GlobalObj.getLoggerObj()
    if not stdoutdata == None:
        for data in stdoutdata:
            logger.debug(data)

def print_stderr(stderrdata):
    logger = GlobalObj.getLoggerObj()
    if not stderrdata == None:
        for data in stderrdata:
            logger.debug(data)

def set_active_volume(volumekey):
    """
    """
    logger = GlobalObj.getLoggerObj()
    env = GlobalObj.getTestenvObj()
    return_status = env.setActiveVolume(volumekey)
    if return_status:
        logger.error("Unable to set Active Volume. \
                    '%s' Not defined in TestEnvironment" % volumekey )
    return return_status

def get_active_volume():
    """
    """
    logger = GlobalObj.getLoggerObj()
    env = GlobalObj.getTestenvObj()
    active_volume = env.getActiveVolume()
    if active_volume is None:
        logger.error("Active Volume not set in the TestEnvironment")
    return active_volume

def attach_timestamp(filename):
    """
    """
    timestamp = time.strftime("%Y_%m_%d_%H_%M_%S")
    new_filename = '.'.join([filename, timestamp])
    return new_filename
