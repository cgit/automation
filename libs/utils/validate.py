import operator
import atfutils
import serverutils
import clientutils
from atfglobals import GlobalObj
import re

def match_md5sum(sum1, sum2):
    """
    Parameters:
        sum1: md5sum1 (type: string)
        sum2: md5sum2 (type: string)

    Description:
        Compares md5sum1 with md5sum2.
        If they are equal, return True. else False
    """
    md5sum_match_status = False
    md5sum_match_status = operator.eq(sum1, sum2)
    return md5sum_match_status

def match_md5sums(md5sums):
    """
    Parameters:
        md5sums: MD5Sums of files
        (type: dict where key: mountkey, value: md5sum (type:string))

    Description:
        Compares md5sums of files.
        If the md5sum of all mounts matches returns True else return False.
    """
    logger = GlobalObj.getLoggerObj()
    md5sum_match_status = False
    compare_key, output = md5sums.popitem()
    compare_value = output["stdoutdata"]

    if not md5sums:
        md5sum_match_status = True
    else:
        for key in md5sums.keys():
            output = md5sums[key]
            value = output["stdoutdata"]
            md5sum_match_status = match_md5sum(compare_value, value)
            if md5sum_match_status is False:
                logger.error("Md5sum of %s didn't match with Md5sum of %s" %
                             (key, compare_key))
                return md5sum_match_status

    return md5sum_match_status

def match_gfid(gfid1, gfid2):
    """
    """
    gfid_match_status = False
    gfid_match_status = operator.eq(gfid1, gfid2)
    return gfid_match_status

def match_gfids(gfid_on_bricks):
    """
    """
    gfid_match_status = False
    compare_key, output = gfid_on_bricks.popitem()
    compare_value = output["stdoutdata"]

    if not gfid_on_bricks:
        gfid_match_status = True
    else:
        for key in gfid_on_bricks.keys():
            output = gfid_on_bricks[key]
            value = output["stdoutdata"]
            gfid_match_status = match_gfid(compare_value, value)
            if gfid_match_status is False:
                logger.error("gfid of %s didn't match gfid of %s" %
                             compare_key, key)
                return gfid_match_status

    return gfid_match_status

def validate_on_bricks(bricks, command, expected_status,
                       expected_output, stream="stdout"):
    """
    Parameters:
        bricks : list of bricks
        command : command to execute on each brick
        expected_status : return status to expect
        expected_output : output to expect
    """
    for brick in bricks:
        output = serverutils.execute_on_brick(brick, command)
        validate_status = atfutils.validate_output(output, expected_status,
                                                   expected_output, stream)
        if validate_status is not 0:
            return validate_status

    return 0

def validate_md5sums(mounts, bricks):
    """
    """
    mounts_md5sums = clientutils.md5sum_of_mounts(mounts)
    bricks_md5sums = serverutils.md5sum_of_bricks(bricks)

    md5sums = mounts_md5sums
    md5sums.update(bricks_md5sums)

    assert_success_status = atfutils.assert_success_of_outputs(md5sums)
    if assert_success_status is not 0:
        return assert_success_status

    md5sum_match_status = match_md5sums(md5sums)
    if md5sum_match_status is False:
        return 1
    else:
        return 0

def validate_gfids(bricks, filename="."):
    """
    """
    gfid_on_bricks = serverutils.get_gfid_on_bricks(bricks, filename)

    assert_success_status = atfutils.assert_success_of_outputs(gfid_on_bricks)
    if assert_success_status is not 0:
        return assert_success_status

    gfid_match_status = match_gfids(gfid_on_bricks)
    if gfid_match_status is False:
        return 1
    else:
        return 0
