"""managerutils module.

Supported Wrappers:-
---------------
*) ssh_connect
*) ssh_connect_allhosts
"""

import re
import ssh
from atfglobals import GlobalObj
import pdb

def ssh_connect(hostkey):
    """
    """
    logger = GlobalObj.getLoggerObj()
    env = GlobalObj.getTestenvObj()
    cm = GlobalObj.getConnectionsManagerObj()
    if cm is None:
        logger.error("Init ConnectionsManager")
        return 1

    host_connection = cm.getConnection(hostkey)
    if not host_connection:
        host_obj = env.getHost(hostkey)
        if not host_obj:
            logger.error("Invalid Host. %s is not defined in TestEnvironment"
                         % hostkey)
            return 1
        else:
            host_connection = ssh.SshConnection()
            if host_connection.connect(host_obj.hostname, host_obj.user,
                                       host_obj.password):
                return 1
            else:
                if re.match("server", hostkey, re.IGNORECASE):
                    cm.addServer(hostkey, host_connection)
                else:
                    cm.addClient(hostkey, host_connection)
                return 0
    else:
        logger.debug("Connection to %s already exist" % hostkey)

    return 0

def ssh_connect_allhosts():
    """
    """
    GlobalObj.initConnectionsManagerObj()
    env = GlobalObj.getTestenvObj()
    cm = GlobalObj.getConnectionsManagerObj()
    hosts_keys = env.getHostsKeys()
    for hostkey in hosts_keys:
        return_status = ssh_connect(hostkey)
        if return_status:
            return return_status

    return 0

__all__ = ['ssh_connect',
           'ssh_connect_allhosts']
