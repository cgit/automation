""" Utilities for Replace Brick Testing
"""
import time
import atfutils
import glusterutils

def wait_till_rb_completes(timeout, serverkey, from_brick, to_brick,
                           expected_message="migration complete"):
    """
    Description: Wait till the replace brick operation completes.
    Parameters:
        timeout: The wait period to check the status of replace brick.
        serverkey : server on which the replace brick status has to be checked.
        from_brick : replace brick operation on from_brick
        to_brick : replace brick operation on to_brick
    """
    operation = "status"
    time.sleep(timeout)
    output = glusterutils.volume_replacebrick(serverkey, from_brick,
                                              to_brick, operation)
    validation_status = atfutils.validate_output(output, 0, expected_message)
    if validation_status is not 0:
        rb_completion_status = 1
    else:
        rb_completion_status = 0

    return rb_completion_status
