"""glusterutils module contains wrappers for gluster commands.
*) glusterd_start
*) glusterd_start_allservers
*) glusterd_stop
*) glusterd_stop_allservers
*) glusterd_restart
*) glusterd_remove_dir
*) glusterd_remove_dir_allservers
*) glusterd_remove_logs_allservers
*) volume_delete
*) volume_create
*) volume_start
*) volume_stop
*) volume_addbrick
*) volume_replacebrick
*) volume_set
*) volume_reset
*) peer_probe
*) create_brick
*) mount_exportdir
*) umount_exportdir
"""

import re
import os
import atfutils
import hostutils
from atfutils import commands
from atfglobals import GlobalObj

def glusterd_start(serverkey, force=False):
    """
    """
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    server_obj = env.getServer(serverkey)
    if server_obj is None:
        logger.error("Invalid Server. %s is not defined in TestEnvironment" %
                     serverkey)
        output["exitstatus"] = 1
        return output
    gluster_version = server_obj.glusterversion

    """ Check if gluster is already running. If already Running and force=True,
    restart glusterd process"""
    command = "ps -e | grep glusterd"
    output = hostutils.execute_command(serverkey, command)
    return_status = atfutils.assert_success(output['exitstatus'])
    if return_status is 0:
        if force:
            output = glusterd_restart(serverkey)
            return output
        else:
            return output

    """ Check the glusterd installed PATH"""
    command = "which glusterd"
    output = hostutils.execute_command(serverkey, command)
    return_status = atfutils.assert_success(output['exitstatus'])
    if return_status is not 0:
        logger.error("Unable to find glusterd PATH")
        return output
    else:
        if output["stdoutdata"]:
            gluster_path = output["stdoutdata"][0].strip("\n")
        else:
            logger.error("Unable to find glusterd PATH")
            return output

        """ Get the glusterd version installed in path gluster_path"""
        if gluster_path:
            command = gluster_path + " --version"
            output = hostutils.execute_command(serverkey, command)
            return_status = atfutils.assert_success(output['exitstatus'])
            if return_status is not 0:
                logger.error("Unable to get glusterd version")
                return output
            else:
                """ Check if the installed glusted version in path gluster_path
                matches with gluster_version specified in testenv"""
                if output["stdoutdata"] is not None:
                    if re.search(gluster_version, str(output["stdoutdata"])):
                        command = gluster_path
                        output = hostutils.execute_command(serverkey, command)
                        return output
                    else:
                        logger.error("Unable to start glusterd")
                        output['exitstatus'] = 1
                        return output
        else:
            logger.error("Unable to find glusterd PATH")
            output['exitstatus'] = 1
            return output

def glusterd_start_allservers(force=False):
    """
    """
    glusterd_start_output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    all_servers = env.getServers()
    for serverkey in all_servers.keys():
        output = glusterd_start(serverkey)
        assert_success_status = atfutils.assert_success(output['exitstatus'])
        if assert_success_status is not 0:
            return output

    glusterd_start_output["exitstatus"] = 0
    glusterd_start_output["stdoutdata"] = "Successful in starting glusterd on all servers"
    return glusterd_start_output

def glusterd_stop(serverkey, signal="SIGTERM"):
    """
    """
    base_command = "pidof glusterd | xargs --no-run-if-empty kill -s %s"
    command = base_command % (signal)
    output = hostutils.execute_command(serverkey, command)
    return output

def glusterd_stop_allservers(signal="SIGTERM"):
    """
    """
    glusterd_stop_output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    all_servers = env.getServers()
    for serverkey in all_servers.keys():
        output = glusterd_stop(serverkey, signal)
        assert_success_status = atfutils.assert_success(output['exitstatus'])
        if assert_success_status is not 0:
            return output

    glusterd_stop_output["exitstatus"] = 0
    glusterd_stop_output["stdoutdata"] = "Successful in stoping glusterd on all servers"
    return glusterd_stop_output

def glusterd_restart(serverkey, signal="SIGTERM"):
    """
    """
    output = glusterd_stop(serverkey, signal)
    if output['exitstatus']:
        return output
    else:
        output = glusterd_start(serverkey)
        return output

def glusterd_remove_dir(serverkey):
    """
    """
    glusterd_dir = GlobalObj.glusterd_dir
    command = ' '.join([commands['unlink_dir_force'], glusterd_dir])
    output = hostutils.execute_command(serverkey, command)
    return output

def glusterd_remove_dir_allservers():
    """
    """
    glusterd_remove_dir_output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    all_servers = env.getServers()
    for serverkey in all_servers.keys():
        output = glusterd_remove_dir(serverkey)
        assert_success_status = atfutils.assert_success(output['exitstatus'])
        if assert_success_status is not 0:
            return output

    glusterd_remove_dir_output["exitstatus"] = 0
    glusterd_remove_dir_output["stdoutdata"] = "Successful in removing glusterd dir on all servers"
    return glusterd_remove_dir_output

def glusterd_remove_logs(serverkey):
    """
    """
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    log_paths = GlobalObj.glusterd_log_paths
    absolute_path_list = []
    prefix_path = ''

    env = GlobalObj.getTestenvObj()
    server_obj = env.getServer(serverkey)
    if server_obj is None:
        logger.error("Invalid Host. %s not defined in TestEnvironment"
                     % serverkey)
        output['exitstatus'] = 1
        return output

    if server_obj.installpath:
        prefix_path = server_obj.installpath

    for path in log_paths:
        absolute_path_list.append(prefix_path + path)

    command = [commands['unlink_dir_force']]
    command.extend(absolute_path_list)
    command = ' '.join(command)
    output = hostutils.execute_command(serverkey, command)
    return output

def glusterd_remove_logs_allservers():
    """
    """
    glusterd_remove_logs_output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    all_servers = env.getServers()
    for serverkey in all_servers.keys():
        output = glusterd_remove_logs(serverkey)
        assert_success_status = atfutils.assert_success(output['exitstatus'])
        if assert_success_status is not 0:
            return output

    glusterd_remove_logs_output["exitstatus"] = 0
    glusterd_remove_logs_output["stdoutdata"] = "Successful in removing glusterd logs on all servers"
    return glusterd_remove_logs_output

def volume_delete(serverkey):
    """
    """
    base_command = "gluster volume delete"
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    active_volume = env.getActiveVolume()
    if not active_volume:
        logger.error("Invalid Volume.ActiveVolume not defined" +
                     "for the TestEnvironment")
        output['exitstatus'] = 1
        return output
    volumename = active_volume.volumename

    command = [base_command]
    command.extend([volumename])
    command = ' '.join(command)
    output = hostutils.execute_command(serverkey, command, commandInput="y\n")
    return output

def volume_create(serverkey):
    """
    """
    base_command = "gluster volume create"
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    active_volume = env.getActiveVolume()
    if not active_volume:
        logger.error("ActiveVolume not defined for the TestEnvironment")
        output['exitstatus'] = 1
        return output
    volumename = active_volume.volumename

    command = [base_command]
    command.extend([volumename])

    if active_volume.replica:
        command.extend(["replica", active_volume.replica])

    if active_volume.stripe:
        command.extend(["stripe", active_volume.stripe])

    if active_volume.transporttype:
        command.extend(["transport", active_volume.transporttype])

    command = ' '.join(command)

    for brick_obj in active_volume.bricks:
        brick_value = brick_obj.hostname + ":" + brick_obj.path
        command = ' '.join([command, brick_value])

    output = hostutils.execute_command(serverkey, command, commandInput="y\n")
    assert_success_status = atfutils.assert_success(output['exitstatus'])
    if assert_success_status is not 0:
        if str(output["stdoutdata"]).rfind("already exists"):
            output = volume_stop(serverkey, force=True)
            output = volume_delete(serverkey)
            assert_success_status = atfutils.assert_success(output['exitstatus'])
            if assert_success_status is not 0:
                return output
            else:
                output = hostutils.execute_command(serverkey, command,
                                                   commandInput="y\n")
                return output
    else:
        return output

def volume_start(serverkey, force=False):
    """
    """
    base_command = "gluster volume start"
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    active_volume = env.getActiveVolume()
    if not active_volume:
        logger.error("ActiveVolume not defined for the TestEnvironment")
        output['exitstatus'] = 1
        return output
    volumename = active_volume.volumename

    command = [base_command]
    command.extend([volumename])
    if force:
        command.extend(["force"])
    command = ' '.join(command)
    output = hostutils.execute_command(serverkey, command, commandInput="y\n")
    assert_success_status = atfutils.assert_success(output['exitstatus'])
    if assert_success_status is not 0:
        if str(output["stdoutdata"]).rfind("already started"):
            output['exitstatus'] = 0
    return output

def volume_stop(serverkey, force=False):
    """
    """
    base_command = "gluster volume stop"
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    active_volume = env.getActiveVolume()
    if not active_volume:
        logger.error("ActiveVolume not defined for the TestEnvironment")
        output['exitstatus'] = 1
        return output
    volumename = active_volume.volumename

    command = [base_command]
    command.extend([volumename])
    if force:
        command.extend(["force"])
    command = ' '.join(command)
    output = hostutils.execute_command(serverkey, command, commandInput="y\n")
    return output

def volume_addbrick(serverkey, *bricks, **arguments):
    """
    """
    base_command = "gluster volume add-brick"
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    active_volume = env.getActiveVolume()
    if not active_volume:
        logger.error("ActiveVolume not defined for the TestEnvironment")
        output['exitstatus'] = 1
        return output
    volumename = active_volume.volumename

    command = [base_command]
    command.extend([volumename])
    """
    arguments can have key brick_type
    brick_type=[<stripe|replica> <COUNT>]
    """
    if arguments.has_key('brick_type'):
        command.extend([arguments['brick_type']])

    for brick in bricks:
        brick_obj = env.getBrick(brick)
        if not brick_obj:
            logger.error("Invalid Brick. Brick Not defined in TestEnvironment")
            output['exitstatus'] = 1
            return output
        brick_value = brick_obj.hostname + ":" + brick_obj.path
        command.extend([brick_value])

    command = ' '.join(command)
    output = hostutils.execute_command(serverkey, command, commandInput="y\n")
    assert_success_status = atfutils.assert_success(output['exitstatus'])
    if assert_success_status is 0:
        if env.addBricksToVolume(*bricks):
            output['exitstatus'] = 1
            return output

    return output

def volume_removebrick(serverkey, *bricks, **arguments):
    """
    *bricks : list of bricks to be removed
    **arguments(optional): It takes key:value pair
                we are using optional keys
                brick_type and operation
    brick_type: [replica <COUNT>]
    operation:{start|pause|abort|status|commit|force}
    """
    base_command = "gluster volume remove-brick"
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    active_volume = env.getActiveVolume()
    if not active_volume:
        logger.error("ActiveVolume not defined in the TestEnvironment")
        output['exitstatus'] = 1
        return output
    volumename = active_volume.volumename

    command = [base_command]
    command.extend([volumename])
    """
    brick_type can have only [replica <COUNT>]
    """
    if arguments.has_key('brick_type'):
        command.extend([arguments['brick_type']])

    for brick in bricks:
        brick_obj = env.getBrick(brick)
        if not brick_obj:
            logger.error("Invalid Brick. Brick not defined in TestEnviroment")
            output['exitstatus'] = 1
            return output
        brick_value = brick_obj.hostname +':'+ brick_obj.path
        command.extend([brick_value])
    """
    operation can have {start|pause|abort|status|commit|force}
    which is optional.
    """
    if arguments.has_key('operation'):
        command.extend([arguments['operation']])

    command = ' '.join(command)
    output = hostutils.execute_command(serverkey, command, commandInput="y\n")
    assert_success_status = atfutils.assert_success(output['exitstatus'])
    if assert_success_status is 0:
        if env.removeBricksFromVolume(*bricks):
            output['exitstatus'] = 1
            return output

    return output

def volume_replacebrick(serverkey, replace_brick, to_brick, operation):
    """
    """
    base_command = "gluster volume replace-brick"
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    active_volume = env.getActiveVolume()
    if not active_volume:
        logger.error("ActiveVolume not defined for the TestEnvironment")
        output['exitstatus'] = 1
        return output
    volumename = active_volume.volumename

    replace_brick_obj = env.getBrick(replace_brick)
    to_brick_obj = env.getBrick(to_brick)
    """
    checking if objects are none
    """
    if not (replace_brick_obj and to_brick_obj):
        logger.error("Invalid Brick. Brick Not defined in TestEnvironment")
        output['exitstatus'] = 1
        return output
    replace_brick_value = replace_brick_obj.hostname + ':' + replace_brick_obj.path
    to_brick_value = to_brick_obj.hostname + ':' + to_brick_obj.path

    command = [base_command]
    command.extend([volumename, replace_brick_value, to_brick_value, operation])
    command = ' '.join(command)
    output = hostutils.execute_command(serverkey, command, commandInput="y\n")
    assert_success_status = atfutils.assert_success(output['exitstatus'])
    if ((assert_success_status is 0) and (operation == "commit")):
        if env.replaceBrickInVolume(replace_brick, to_brick):
            output['exitstatus'] = 1
            return output

    return output

def volume_set(serverkey, key, value):
    """
    """
    base_command = "gluster volume set"
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    active_volume = env.getActiveVolume()
    if not active_volume:
        logger.error("ActiveVolume not defined for the TestEnvironment")
        output['exitstatus'] = 1
        return output
    volumename = active_volume.volumename

    command = [base_command]
    command.extend([volumename, key, value])
    command = ' '.join(command)
    output = hostutils.execute_command(serverkey, command, commandInput="y\n")
    return output

def volume_log_rotate(serverkey, brick):
    """
    brick is compulsory parameter
    """
    base_command = "gluster volume log rotate"
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    active_volume = env.getActiveVolume()
    if not active_volume:
        logger.error("ActiveVolume not defined for the Testenvironment")
        output['exitstatus'] = 1
        return output
    volumename = active_volume.volumename

    brick_obj = env.getBrick(brick)
    if not brick_obj:
        logger.error("Invalid Brick. Brick Not defined in TestEnvironment")
        output['exitstatus'] = 1
        return output
    brick_value = brick_obj.hostname + ":" + brick_obj.path

    command = [base_command]
    command.extend([volumename, brick_value])
    command = ' '.join(command)
    output = hostutils.execute_command(serverkey, command, commandInput="y\n")
    return output

def volume_reset(serverkey):
    """
    """
    base_command = "gluster volume reset "
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    active_volume = env.getActiveVolume()
    if not active_volume:
        logger.error("ActiveVolume not defined for the TestEnvironment")
        output['exitstatus'] = 1
        return output
    volumename = active_volume.volumename

    command = [base_command]
    command.extend([volumename])
    command = ' '.join(command)
    output = hostutils.execute_command(serverkey, command, commandInput="y\n")
    return output

def volume_geo_replication(serverkey, slavekey, operation, volume=False,
                           local=False, *options):
    """
    operation: {start|stop|config|status|log-rotate}
    options are valid oly if the operation is 'config'
    ex: log_file /usr/local/var/log/glusterfs/geo.log
        remote_gsyncd /usr/local/libexec/glusterfs/gsyncd
    if you are starting geo-replication session with
    another volume, you need to give volume = True
    and by default it take path given in the configuration file
    """
    output = atfutils.get_new_output_obj()
    logger = GlobalObj.getLoggerObj()
    base_command = "gluster volume geo-replication"
    env = GlobalObj.getTestenvObj()
    command = [base_command]

    active_volume = env.getActiveVolume()
    if not active_volume:
        logger.error("ActiveVolume not defined for the TestEnvironment")
        output['exitstatus'] = 1
        return output

    volumename = active_volume.volumename

    slave_obj = env.getSlave(slavekey)
    if not slave_obj:
        logger.error("Invalid slave. Slave not defined in Testenvironment")
        output['exitstatus'] = 1
        return ouput
    if not local:
        if not volume:
            slave_value = slave_obj.hostname +":"+ slave_obj.path
        else:
            slave_value = slave_obj.hostname +":"+ slave_obj.volumename
    else:
        if not volume:
            slave_value = slave_obj.path
        else:
            slave_value = ":"+slave_obj.volumename

    command.extend([volumename, slave_value])

    if operation == 'config':
        if options:
            command.extend(['config', options[0]])
        else:
            command.extend(['config'])
    else:
        command.extend([operation])

    command = ' '.join(command)

    output = hostutils.execute_command(serverkey, command, commandInput="y\n")
    return output


def volume_profile(serverkey, operation):
    """
    operation:{start|info|stop}
    """
    base_command = "gluster volume profile"
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    active_volume = env.getActiveVolume()
    if not active_volume:
        logger.error("ActiveVolume not defined for the TestEnvironment")
        output['exitstatus'] = 1
        return output
    volumename = active_volume.volumename

    command = [base_command]
    command.extend([volumename, operation])
    command = ' '.join(command)
    output = hostutils.execute_command(serverkey, command, commandInput="y\n")
    return output


def volume_quota(serverkey, operation, **arguments):
    """
    arguments can have two values
    path: path can be '/'
        ex: path='/'
    value: value can be in GB or MB
        ex: value=1GB
    """
    base_command = "gluster volume quota"
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    active_volume = env.getActiveVolume()
    if not active_volume:
        logger.error("ActiveVolume not defined for the Testenvironment")
        output['exitstatus'] = 1
        return output
    volumename = active_volume.volumename

    command = [base_command]
    command.extend([volumename, operation])

    if arguments.has_key('path'):
        command.extend([arguments['path']])

    if arguments.has_key('value'):
        command.extend([arguments['value']])

    command = ' '.join(command)
    output = hostutils.execute_command(serverkey, command, commandInput="y\n")
    return output


def volume_top(serverkey, operation, **arguments):
    """
    operation:{[open|read|write|opendir|readdir] |[read-perf|write-perf
                bs <size> count <count>]}
    arguments(optional): Takes maximum two parameters
    brick : brick1, brick2 etc
    list-cnt: can take any number

    """
    base_command = "gluster volume top"
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    active_volume = env.getActiveVolume()
    if not active_volume:
        logger.error("ActiveVolume not defines for the TestEnvironment")
        output['exitstatus'] = 1
        return output
    volumename = active_volume.volumename

    command = [base_command]
    command.extend([volumename, operation])

    if arguments.has_key('brick'):
        brick_obj = env.getBrick(arguments['brick'])
        if not brick_obj:
            logger.error("Invalid Brick. Brick Not defined in TestEnvironment")
            output['exitstatus'] = 1
            return output
        brick_value = brick_obj.hostname+':'+brick_obj.path
        command.extend(['brick',brick_value])

    if arguments.has_key('list_cnt'):
        command.extend(['list-cnt', arguments['list_cnt']])

    command = ' '.join(command)
    output = hostutils.execute_command(serverkey, command, commandInput="y\n")
    return output

def volume_stop_brick(brickkey, signal="SIGTERM"):
    """
    Description: Stop a brick
    Arguments: brickkey. (The brick to be stopped. Ex:- "brick1")
    """
    base_command = "kill -s %s $(cat %s)"
    glusterd_vol_dir = "/etc/glusterd/vols"
    brick_pid_dir = "run"
    pid_file_extension = ".pid"

    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    active_volume = env.getActiveVolume()
    if not active_volume:
        logger.error("ActiveVolume not defines for the TestEnvironment")
        output['exitstatus'] = 1
        return output
    volumename = active_volume.volumename

    """ Get the serverkey on which it has to be executed
    """
    raw_brick_obj = env.getRawBrick(brickkey)
    if not raw_brick_obj:
        logger.error("InValid Brick. %s not defined in TestEnvironment"
                     % brickkey)
        output['exitstatus'] = 1
        return output
    serverkey = re.split("\.", raw_brick_obj.hostname, maxsplit=1)[0]

    """ Get the brickobj
    """
    brick_obj = env.getBrick(brickkey)
    if not brick_obj:
        logger.error("Invalid Brick. Brick Not defined in TestEnvironment")
        output['exitstatus'] = 1
        return output

    """ Absolute path for the file containing the pid of brick process
    """
    hostname = brick_obj.hostname
    path = brick_obj.path.replace("/", "-")
    pid_filename = hostname + path + pid_file_extension
    pid_file_abspath = os.path.join(glusterd_vol_dir, volumename,
                            brick_pid_dir, pid_filename)

    command = base_command % (signal, pid_file_abspath)
    output = hostutils.execute_command(serverkey, command, commandInput="y\n")
    return output

def peer_probe(from_serverkey):
    """
    """
    base_command = "gluster peer probe"
    peer_probe_output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    all_servers = env.getServers()
    all_servers.pop(from_serverkey)
    for serverkey in all_servers.keys():
        if not serverkey is from_serverkey:
            server_obj = all_servers[serverkey]
            """
            One hostname is being taken at a time while executing peer probe
            """
            command = ' '.join([base_command, server_obj.hostname])
            output = hostutils.execute_command(from_serverkey, command,
                                               commandInput="y\n")
            assert_success_status = atfutils.assert_success(output['exitstatus'])
            if assert_success_status is not 0:
                return output

    peer_probe_output["exitstatus"] = 0
    peer_probe_output["stdoutdata"] = "Peer Probe Successful for all servers"
    return peer_probe_output

def create_brick(brickkey):
    """
    """
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    brick_obj = env.getRawBrick(brickkey)
    if not brick_obj:
        logger.error("Invalid brick. Brick not defined in Testenvironment")
        output['exitstatus'] = 1
        return output
    hostname_value = brick_obj.hostname
    serverkey = re.split("\.", hostname_value, maxsplit=1)[0]
    exportdir = brick_obj.path

    device = fstype = None
    """If the exportdir is not a mount point of a device:
    1) Remove the existing exportdir
    2) Create new exportdir"""
    if re.match("^\/", exportdir):
        dirpath = exportdir
    else:
        export_obj = env.getExportdir(exportdir)
        dirpath = export_obj.dir
        device = export_obj.device
        fstype = export_obj.fstype
        options = export_obj.options

    server_obj = env.getServer(serverkey)
    if server_obj is None:
        logger.error("Invalid Host. %s not defined in TestEnvironment"
                     % serverkey)
        output['exitstatus'] = 1
        return output
    server_hostname = server_obj.hostname

    logger.debug('%s: Executing Command: %s' %(server_hostname, 'create_brick'))
    if device:
        output = hostutils.umount_device(serverkey, device)
        assert_success_status = atfutils.assert_success(output['exitstatus'])
        if assert_success_status is not 0:
            return output

        output = hostutils.mkfs(serverkey, device, fstype, options)
        assert_success_status = atfutils.assert_success(output['exitstatus'])
        if assert_success_status is not 0:
            return output

        output = hostutils.mount(serverkey, device, fstype, dirpath, options)
        assert_success_status = atfutils.assert_success(output['exitstatus'])
        if assert_success_status is not 0:
            return output
    else:
        output = hostutils.rmdir(serverkey, dirpath)
        assert_success_status = atfutils.assert_success(output['exitstatus'])
        if assert_success_status is not 0:
            return output

        output = hostutils.mkdir(serverkey, dirpath)
        assert_success_status = atfutils.assert_success(output['exitstatus'])
        if assert_success_status is not 0:
            return output

    output["exitstatus"] = 0
    output["stdoutdata"] = "Successfully Created Brick %s" % brickkey
    return output

def create_brick_allservers():
    """
    """
    create_brick_output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    brick_keys = env.getBrickKeys()
    for brickkey in brick_keys:
        output = create_brick(brickkey)
        assert_success_status = atfutils.assert_success(output['exitstatus'])
        if assert_success_status is not 0:
            return output

    create_brick_output["exitstatus"] = 0
    create_brick_output["stdoutdata"] = "Successful in creating bricks on all servers"
    return create_brick_output

def create_slave(slavekey):
    """
    This function creates the slave. In the sense ,
    if it is just some path, its not it leave as it is.
    If path in slave section is given as some exportdir
    then it will format the device given in the exportdir section
    with the file system and mounts the device on the path ,
    both defined in the exportdir section in configuration file
    """
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    slave_obj = env.getRawSlave(slavekey)
    if not slave_obj:
        logger.error("Invalid slave.Slave not defined in Testenvironment")
        output['exitstatus'] = 1
        return output
    hostname_value = slave_obj.hostname
    serverkey = re.split("\.", hostname_value, maxsplit=1)[0]
    exportdir = slave_obj.path

    device = fstype = None
    """If the exportdir is not a mount point of a device:
    1) Remove the existing exportdir
    2) Create new exportdir"""
    if re.match("^\/", exportdir):
        dirpath = exportdir
    else:
        export_obj = env.getExportdir(exportdir)
        dirpath = export_obj.dir
        device = export_obj.device
        fstype = export_obj.fstype
        options = export_obj.options

    server_obj = env.getServer(serverkey)
    if server_obj is None:
        logger.error("Invalid Host. %s not defined in TestEnvironment"
                     % serverkey)
        output['exitstatus'] = 1
        return output
    server_hostname = server_obj.hostname

    logger.debug('%s: Executing Command: %s' %(server_hostname, 'create_slave'))
    if device:
        output = hostutils.umount_device(serverkey, device)
        assert_success_status = atfutils.assert_success(output['exitstatus'])
        if assert_success_status is not 0:
            return output

        output = hostutils.mkfs(serverkey, device, fstype, options)
        assert_success_status = atfutils.assert_success(output['exitstatus'])
        if assert_success_status is not 0:
            return output

        output = hostutils.mount(serverkey, device, fstype, dirpath, options)
        assert_success_status = atfutils.assert_success(output['exitstatus'])
        if assert_success_status is not 0:
            return output
    else:
        output = hostutils.rmdir(serverkey, dirpath)
        assert_success_status = atfutils.assert_success(output['exitstatus'])
        if assert_success_status is not 0:
            return output

        output = hostutils.mkdir(serverkey, dirpath)
        assert_success_status = atfutils.assert_success(output['exitstatus'])
        if assert_success_status is not 0:
            return output

    output["exitstatus"] = 0
    output["stdoutdata"] = "Successfully Created Slave %s" % slavekey
    return output

def create_slave_allservers():
    """
    """
    create_slave_output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    slave_keys = env.getSlaveKeys()
    for slavekey in slave_keys:
        output = create_slave(slavekey)
        assert_success_status = atfutils.assert_success(output['exitstatus'])
        if assert_success_status is not 0:
            return output

    create_slave_output["exitstatus"] = 0
    create_slave_output["stdoutdata"] = "Successful in creating bricks on all \
servers"
    return create_slave_output
