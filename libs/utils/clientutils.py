"""clientutils module contains functions required for performing
certain operations on client

Supported Wrappers :-
-----------
*) umount
*) umountall
*) mount
*) mountall
"""

import atfutils
import hostutils
from atfutils import commands
from atfglobals import GlobalObj

def umount(mountkey):
    """unmounts a mountpoint

    Parameters:
        mountkey : name given to a mount as specified in testenv.cfg file.
        Ex:-"mount1"

    Returns:
        Success : 0
        Failure : 1`
    """
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    mount_obj = env.getMount(mountkey)
    if not mount_obj:
        logger.error("InValid Mount. '%s' not defined in TestEnvironment"
                     % mountkey)
        output["exitstatus"] = 1
        return output

    clientkey = mount_obj.client
    command = ' '.join([commands['umount'], mount_obj.dir])
    output = hostutils.execute_command(clientkey, command)
    assert_success_status = atfutils.assert_success(output['exitstatus'])
    if assert_success_status is not 0:
        stdoutdata = str(output["stdoutdata"])
        if ((stdoutdata.rfind("not found")) or (stdoutdata.rfind("not mount"))):
            output["exitstatus"] = 0
        else:
            logger.error("Unable to umount %s" % mountkey)

    return output

def umountall():
    """unmounts all mount specified in testenv.cfg file.
        Ex:- mount1, mount2 etc

    Parameters:
        None

    Returns:
        Success : 0
        Failure : 1`
    """
    umountall_output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    mounts_keys = env.getMountsKeys()
    for mountkey in mounts_keys:
        output = umount(mountkey)
        assert_success_status = atfutils.assert_success(output['exitstatus'])
        if assert_success_status is not 0:
            return output

    umountall_output["exitstatus"] = 0
    umountall_output["stdoutdata"] = "Successful in unmounting all mounts"
    return umountall_output

def mount(mountkey):
    """mounts a filesystem

    Parameters:
        mountkey : name given to a mount as specified in testenv.cfg file.
        Ex:-"mount1"

    Returns:
        Success : 0
        Failure : 1`
    """
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    mount_obj = env.getMount(mountkey)
    if not mount_obj:
        logger.error("InValid Mount. %s not defined in TestEnvironment"
                     % mountkey)
        output["exitstatus"] = 1
        return output

    #mount command
    command = [commands['mount']]

    #mount type
    command.extend(["-t", mount_obj.type])

    #mount options if specified
    if mount_obj.options:
        command.extend(["-o", mount_obj.options])

    #mount device
    mountdevice_obj = mount_obj.device
    device = mountdevice_obj.hostname + ":/" + mountdevice_obj.volumename
    command.extend([device, mount_obj.dir])
    command = ' '.join(command)

    clientkey = mount_obj.client
    #create mount directory
    output = hostutils.mkdir(clientkey, mount_obj.dir)
    assert_success_status = atfutils.assert_success(output["exitstatus"])
    if assert_success_status is not 0:
        return output

    #execute mount command
    output = hostutils.execute_command(clientkey, command)
    return output

def mountall():
    """mounts a filesystem for all mounts specified in testenv.cfg file.

    Parameters:
        None

    Returns:
        Success : 0
        Failure : 1`
    """
    mountall_output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    mounts_keys = env.getMountsKeys()
    for mountkey in mounts_keys:
        output = mount(mountkey)
        assert_success_status = atfutils.assert_success(output['exitstatus'])
        if assert_success_status is not 0:
            return output

    mountall_output["exitstatus"] = 0
    mountall_output["stdoutdata"] = "Successful in mounting all mounts"
    return mountall_output

def md5sum_of_mount(mountkey):
    """
    Parameter: mount (tye: string)
    Returns: output of arequal-checksum command execution(type:dict)
        Key : Value of the Output
        exitstatus: exit status of the arequal-checksum command on mount
        stdoutdata: stdout data of arequal-checksum command execution
        stderrdata: stderr data of arequal-checksum command execution
    """
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    mount_obj = env.getMount(mountkey)
    if not mount_obj:
        logger.error("InValid Mount. %s not defined in TestEnvironment"
                     % mountkey)
        output["exitstatus"] = 1
        return output

    clientkey = mount_obj.client
    path = mount_obj.dir
    output = hostutils.md5sum(clientkey, path)
    return output

def md5sum_of_mounts(mounts):
    """
    Description:
        Calculate md5sum of mounts using arequal-checksum

    Parameters: mounts (type: List)

    Returns: md5sums of all the mounts (type: dict)
        Keys: mounts
        Value: ouput (type:dict)
            exitstatus: exit status of the arequal-checksum command on mount
            stdoutdata: stdout data of arequal-checksum command execution
            stderrdata: stderr data of arequal-checksum command execution
    """
    md5sums = {}
    for mountkey in mounts:
        output = md5sum_of_mount(mountkey)
        md5sums[mountkey] = output

    return md5sums

def execute_on_mount(mountkey, command, commandInput=None):
    """
    """
    logger = GlobalObj.getLoggerObj()
    output = atfutils.get_new_output_obj()

    env = GlobalObj.getTestenvObj()
    mount_obj = env.getMount(mountkey)
    if not mount_obj:
        logger.error("InValid Mount. %s not defined in TestEnvironment"
                     % mountkey)
        output["exitstatus"] = 1
        return output

    clientkey = mount_obj.client
    mountdir = mount_obj.dir
    command = "cd %s ; %s" % (mountdir , command)
    output = hostutils.execute_command(clientkey, command, commandInput)
    return output

def execute_on_mounts(mounts, command, commandInput=None):
    """
    """
    all_outputs = {}

    for mountkey in mounts:
        output = execute_on_mount(mountkey, command, commandInput)
        all_outputs[mountkey] = output

    return all_outputs
