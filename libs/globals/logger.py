"""
 logger class contains variables and methods for Logging 
 the events during the test run.
"""
import logging

class Log(logging.Logger):
    _summarylogformat = '%(asctime)s %(levelname)s  : %(filename)s %(lineno)d  - \'%(message)s\''
    
    _detaillogformat = '%(asctime)s %(levelname)s  : %(pathname)s %(funcName)s %(lineno)d - \'%(message)s\''
    
    _stdoutlogformat = '%(asctime)s %(levelname)s : %(filename)s %(lineno)d - \'%(message)s\''
    
    _handlers = {}

    def getLogLevel(self, loglevel):
        """
        Description:
            Returns logging.(LogLevel)  for loglevel

        Parameters:
            loglevel: String specifying the loglevel

        Returns:
            logging.(Loglevel) 
        """

        LEVELS = {'DEBUG': logging.DEBUG,
                  'INFO': logging.INFO,
                  'WARNING': logging.WARNING,
                  'ERROR': logging.ERROR,
                  'CRITICAL': logging.CRITICAL,
                  'debug': logging.DEBUG,
                  'info': logging.INFO,
                  'warning': logging.WARNING,
                  'error': logging.ERROR,
                  'critical': logging.CRITICAL}
        return LEVELS.get(loglevel, logging.NOTSET)
            
    def addSummarylogHandler(self, filename, loglevel='INFO', mode='w'):
        """
        Description:
            # Create Summary Log File Handler
            # Set the Log Level
            # Set the Log Record format for Summary Log

        Returns:
            Success: 0
            Failure: 1
        """ 
        loglevel = self.getLogLevel(loglevel)
        try:
            summary_handler = logging.FileHandler(filename, mode)

        except IOError as (errno, errstr):
            return 1

        else:
            summary_formatter = logging.Formatter(self._summarylogformat)
            summary_handler.setFormatter(summary_formatter)
            summary_handler.setLevel(loglevel)
            self.addHandler(summary_handler)
            self._handlers['summarylog'] = summary_handler

        return 0

    def addStdoutlogHandler(self, loglevel='INFO'):
        """
        Description:
            # Create Stdout Log StreamHandler 
            # Set the Log Level
            # Set the Log Record format for STDOUT
        """
        loglevel = self.getLogLevel(loglevel)
        stdout_handler = logging.StreamHandler()
        stdout_handler.setLevel(loglevel)
        stdout_formatter = logging.Formatter(self._stdoutlogformat)
        stdout_handler.setFormatter(stdout_formatter)
        self.addHandler(stdout_handler)
        self._handlers['stdoutlog'] = stdout_handler
        
        return 0

    def addDetaillogHandler(self, filename, loglevel='DEBUG', mode='w'):
        """
        Description:
            #Add a Detail Log FileHandler
            #Set Log Level
            #Set Log Record format for DetailLog

        Returns:
            Success: 0
            Failure: 1
        """
        loglevel = self.getLogLevel(loglevel)
        
        try:
            detail_handler = logging.FileHandler(filename, mode='w')

        except IOError as (errno, errstr):
            return 1

        else:
            detail_handler.setLevel(loglevel)
            detail_formatter = logging.Formatter(self._detaillogformat)
            detail_handler.setFormatter(detail_formatter)
            self.addHandler(detail_handler)
            self._handlers['detaillog'] = detail_handler
            
        return 0

    def removelogHandler(self, logname):
        """
        Description:
            Remove LogHandler
            
        Parameters:
            logname: Name of the Logger (summarylog/ detaillog/ stdoutlog)
        """
        if self._handlers.has_key(logname):
            log_handler = self._handlers.pop(logname)
            self.removeHandler(log_handler)              

        return 0
    
    
__all__ = ['Log']
