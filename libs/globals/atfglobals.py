"""atfglobals module contain AtfGlobal class and GlobalObj

AtfGlobals class wrapps all global objects used in the framework
*) TestrunInfo
*) Logger
*) Testenv
*) ConnectionsManager

GlobalObj is 'The Instance' of AtfGlobals which will be referred throughout
the framework utilities.
"""

import testruninfo
import logger
import testenv
import manager

class AtfGlobals:
    def __init__(self):
        self._testruninfo = None
        self._logger = None
        self._env = None
        self._connectionsmanager = None
        self.testrunid = None
        self.logname = "ATFLOG"
        self.atfdir = None
        self.testruninfo_file = None
        self.summarylog_file = "summarylog.out"
        self.summarylog_level = 'INFO'
        self.detaillog_file = "detaillog.out"
        self.detaillog_level = 'DEBUG'
        self.attach_timestamp = 'yes'
        self.stdoutlog_dolog = 'yes'
        self.stdoutlog_level = 'INFO'
        self.testunits_maindir = "TestUnits"
        self.testunit_mainmodule = "testunit"
        self.testenv_file = "testenv.cfg"
        self.testcaseslist_file = "testcaseslist"
        self.glusterd_dir = "/etc/glusterd/*"
        self.glusterd_log_paths = ["/var/log/glusterfs/*.log",
                                   "/var/log/glusterfs/bricks/*"]


    def initLoggerObj(self):
        """Instantiation of Logger Object
        """
        self._logger = logger.Log(self.logname)

    def getLoggerObj(self):
        """Returns Logger Object
        """
        return self._logger

    def initConnectionsManagerObj(self):
        """Instantiation of ConnectionsManager Object
        """
        self._connectionsmanager = manager.ConnectionsManager()

    def getConnectionsManagerObj(self):
        """Returns ConnectionsManager Object
        """
        return self._connectionsmanager

    def initTestrunInfoObj(self):
        """Instantiation of TestrunInfo Object
        """
        self._testruninfo = testruninfo.TestRunInfo()

    def getTestrunInfoObj(self):
        """Returns TestrunInfo Object
        """
        return self._testruninfo

    def initTestenvObj(self):
        """Instantiation of Testenv Object
        """
        self._env = testenv.TestEnv()

    def getTestenvObj(self):
        """Returns Current TestEnvironment Object.
        """
        return self._env

GlobalObj = AtfGlobals()
__all__ = ['GlobalObj']
