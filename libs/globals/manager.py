""" manager module contains:

*) ConnectionsManager Class
"""
class ConnectionsManager():
    """
    *) Manages Client SSH Connections in the current TestEnvironment
    *) Manages Server SSH Connections in the current TestEnvironment
    *) Manages allhosts SSH Connection in the current TestEnvironment
    """
    def __init__(self):

        self._serverpool = {}
        self._clientpool = {}
        self._all = {}

    def addServer(self, key, server):
        """
        Add a server to _serverpool
        """

        self._serverpool[key] = server
        self._all[key] = server
        return

    def addClient(self, key, client):
        """
        Add a client to clientpool
        """

        self._clientpool[key] = client
        self._all[key] = client
        return

    def getServers(self):
        """
        Return the server object
        """

        return self._serverpool

    def getClients(self):
        """
        Return the client object
        """

        return self._clientpool

    def getConnection(self, key):
        """
        """
        value = None
        if self._all.has_key(key):
            value = self._all[key]
        return value

    def getConnections(self):
        """
        """
        return self._all

    
    
        
