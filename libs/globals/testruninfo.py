"""testruninfo module

TestRunInfo Class contains variables and methods for storing and retrieving 
information about current "TestRun". 
"""
import re

class TestRunInfo():
    def __init__(self):
        self._testunits = []
        self._keywords = ''
        self._glusterversion = ''

    def addGlusterVersion(self, version):
        """
        """
        self._glusterversion = version

    def getGlusterVersion(self):
        """
        """
        return self._glusterversion
        
    def addTestUnits(self, testunit):
        """
        Description:
            Add a testunit to TestUnits List

        Parameter:
            testunit: Name of the Testing Unit

        Returns:
        """

        self._testunits.append(testunit)
        return

    def getTestUnits(self):
        """
        Description:
            Return TestUnits List

        Parameters:

        Returns:
            Success: Testunit Name
            Failure: ''
        """

        return self._testunits
    
    def addKeywords(self, keywords):
        """
        Description:
            Add Keywords to KeyWords List

        Parameters:
            keyname: Keyword

        Returns:
        """
        self._keywords = keywords

    def getKeywords(self):
        """
        """
        return self._keywords
    
    
    
