"""testenv module.

TestEnv Class has attributes which holds information of the current
testenvironment. It includes information about Servers, Clients, Volume,
Bricks, MountPoints etc.
TestEnv Class provides methods to access the testenvironment info
"""

from collections import namedtuple
import re

class TestEnv():

    def __init__(self):
        self._exportdirs = {}
        self._servers = {}
        self._bricks = {}
        self._volumes = {}
        self._clients = {}
        self._mountdevices = {}
        self._mounts = {}
        self._slaves = {}
        self._gluster_download_paths = []
        self._active_volume = None

        self._exportdir_tuple = namedtuple('ExportDir',
                                           ['dir', 'fstype', 'device', 'options'])

        self._server_tuple = namedtuple('Server',
                                        ['hostname', 'user', 'password',
                                         'glusterversion', 'installpath'])

        self._brick_tuple = namedtuple('Brick',
                                       ['hostname', 'path'])

        self._volume_tuple = namedtuple('Volume',
                                        ['volumename','replica', 'stripe',
                                         'transporttype', 'bricks'])

        self._client_tuple = namedtuple('Client',
                                        ['hostname', 'user', 'password',
                                         'glusterversion', 'installpath'])

        self._mountdevice_tuple = namedtuple('MountDevice',
                                             ['hostname', 'volumename'])

        self._mount_tuple = namedtuple('Mount',
                                       ['client', 'dir', 'device',
                                        'type', 'logfile', 'options'])

        self._slave_tuple = namedtuple('slave',
                                       ['hostname','path','volumename'])

    def addExportdir(self, key, dir_, **arguments):
        """
        """
        fstype = None
        device = None
        options = None
        if (arguments.has_key('fstype') and arguments['fstype']):
            fstype = arguments['fstype']

        if (arguments.has_key('device') and arguments['device']):
            device = arguments['device']

        if (arguments.has_key('options') and arguments['options']):
            options = arguments['options']

        exportdir_obj = self._exportdir_tuple(dir_, fstype, device, options)
        self._exportdirs[key] = exportdir_obj

    def getExportdir(self, exportdirkey):
        """
        """
        exportdir_obj = None
        if self._exportdirs.has_key(exportdirkey):
            exportdir_obj = self._exportdirs[exportdirkey]

        return exportdir_obj

    def getExportdirs(self):
        """Returns self._exportdirs dictionary.
        'key' in dict is exportdirkey
        'value' is exportdir namedtuple object
        """

        return self._exportdirs

    def addServer(self, key, hostname, user, password,
                  glusterversion, **arguments):
        """
        """
        installpath = None
        if (arguments.has_key('installpath') and arguments['installpath']):
            installpath = arguments['installpath']

        server_obj = self._server_tuple(hostname, user, password,
                                        glusterversion, installpath)

        self._servers[key] = server_obj

    def getServer(self, serverkey):
        """
        """
        server_obj = None
        if self._servers.has_key(serverkey):
            server_obj = self._servers[serverkey]

        return server_obj

    def getServers(self):
        """
        """
        servers = {}

        for serverkey in self._servers.keys():
            servers[serverkey] = self.getServer(serverkey)

        return servers

    def addClient(self, key, hostname, user, password,
                  glusterversion, **arguments):
        """
        """
        installpath = None
        if arguments.has_key('installpath') and arguments['installpath']:
            installpath = arguments['installpath']

        client_obj = self._client_tuple(hostname, user, password,
                                        glusterversion, installpath)
        self._clients[key] = client_obj

    def getClient(self, clientkey):
        """
        """
        client_obj = None
        if self._clients.has_key(clientkey):
            client_obj = self._clients[clientkey]

        return client_obj

    def getClients(self):
        """
        """
        clients = {}

        for clientkey in self._clients.keys():
            clients[clientkey] = self.getClient(clientkey)

        return clients

    def addBrick(self, key, hostname, path, **arguments):
        """
        """
        brick_obj = self._brick_tuple(hostname, path)
        self._bricks[key] = brick_obj

    def getBrick(self, brickkey):
        """
        """
        return_brick_obj = None
        newhostname = newpath = ''

        if self._bricks.has_key(brickkey):
            brick_obj = self._bricks[brickkey]
        else:
            return return_brick_obj


        hostname_value = brick_obj.hostname
        serverkey = re.split("\.", hostname_value, maxsplit=1)[0]
        server_obj = self.getServer(serverkey)
        if server_obj:
            newhostname = server_obj.hostname
        else:
            return return_brick_obj

        path_value = brick_obj.path
        if re.match("^\/", path_value):
            newpath = path_value
        else:
            exportdir_obj = self.getExportdir(path_value)
            if exportdir_obj:
                newpath = exportdir_obj.dir
            else:
                brick_obj = None
                return return_brick_obj

        return_brick_obj = brick_obj._replace(hostname=newhostname,
                                              path=newpath)

        return return_brick_obj

    def getBricks(self):
        """
        """
        return_bricks = {}

        for brickkey in self._bricks.keys():
            return_bricks[brickkey] = self.getBrick(brickkey)

        return return_bricks

    def getRawBrick(self, brickkey):
        brick_obj = None
        if self._bricks.has_key(brickkey):
            brick_obj = self._bricks[brickkey]
        return brick_obj

    def getBrickKeys(self):
        """
        """
        brick_keys = []
        brick_keys.extend(self._bricks.keys())
        return brick_keys

    def addBricksToVolume(self, *bricks, **arguments):
        """
        Adds the added brick to volume object
        """
        volume_obj = None

        if arguments.has_key("volumekey"):
            volumekey = arguments[volumekey]
        else:
            volumekey = self._active_volume

        if not (volumekey and self._volumes.has_key(volumekey)):
            return 1

        volume_obj = self._volumes[volumekey]

        for brick in bricks:
            volume_obj.bricks.append(brick)

        return 0

    def removeBricksFromVolume(self, *bricks, **arguments):
        """
        Removes the removed brick from volume object
        """
        volume_obj = None

        if arguments.has_key("volumekey"):
            volumekey = arguments['volumekay']
        else:
            volumekey = self._active_volume

        if not (volumekey and self._volumes.has_key(volumekey)):
            return 1

        volume_obj = self._volumes[volumekey]

        for brick in bricks:
            volume_obj.bricks.remove(brick)

        return 0


    def replaceBrickInVolume(self, replace_brick, to_brick,
                             volumekey="ActiveVolume"):
        """
        """
        volume_obj = None
        replaced_status = False
        if volumekey == "ActiveVolume":
            volumekey = self._active_volume

        if not (volumekey and self._volumes.has_key(volumekey)):
            return 1

        volume_obj = self._volumes[volumekey]
        for index, brick in enumerate(volume_obj.bricks):
            if brick == replace_brick:
                volume_obj.bricks[index] = to_brick
                replaced_status = True
                break
            else:
                continue

        if replaced_status:
            return 0
        else:
            return 1

    def addVolume(self, key, volumename, replica,
                  stripe, transporttype, bricks):
        """
        """
        brickskeylist = [x.strip() for x in bricks.split(",")]
        volume_obj = self._volume_tuple(volumename, replica, stripe,
                                        transporttype, brickskeylist)
        self._volumes[key] = volume_obj

    def getVolume(self, volumekey):
        """
        """
        return_volume_obj = None

        if not self._volumes.has_key(volumekey):
            return return_volume_obj

        volume_obj = self._volumes[volumekey]
        brickslist = []
        for brickkey in volume_obj.bricks:
            brick_obj = self.getBrick(brickkey)
            if not brick_obj:
                return return_volume_obj
            else:
                brickslist.append(brick_obj)

        return_volume_obj = volume_obj._replace(bricks=brickslist)

        return return_volume_obj

    def getVolumes(self):
        """
        """
        return_volumes = {}
        for volumekey in self._volumes.keys():
            return_volumes[volumekey] = self.getVolume(volumekey)

        return return_volumes

    def addMountDevice(self, key, hostname, volumename):
        """
        """
        mountdevice_obj = self._mountdevice_tuple(hostname, volumename)
        self._mountdevices[key] = mountdevice_obj

    def getMountDevice(self, mountdevicekey):
        """
        *) Check the hostname = IPAddress
        *) Check hostname refers to any server
        *) Check the volume is defined.
        *) Substitute all values
        """
        returndevice_obj = None
        ip_pattern = re.compile('(?:[\d]{1,3})\.(?:[\d]{1,3})\.(?:[\d]{1,3})\.(?:[\d]{1,3})')

        if not self._mountdevices.has_key(mountdevicekey):
            return returndevice_obj

        else:
            mountdevice_obj = self._mountdevices[mountdevicekey]

            hostname_value = mountdevice_obj.hostname
            if ip_pattern.match(hostname_value):
                newhostname = hostname_value
            elif re.match('(([a-z]|[A-Z])+[0-9]+)\.hostname', hostname_value):
                serverkey = re.split("\.", hostname_value, maxsplit=1)[0]
                server_obj = self.getServer(serverkey)
                if server_obj:
                    newhostname = server_obj.hostname
                else:
                    return returndevice_obj
            else:
                newhostname = hostname_value
            volumekey = re.split("\.", mountdevice_obj.volumename, maxsplit=1)[0]
            volume_obj = self.getVolume(volumekey)
            if volume_obj:
                newvolumename = volume_obj.volumename
            else:
                return returndevice_obj


            returndevice_obj = mountdevice_obj._replace(hostname=newhostname,
                                                        volumename=newvolumename)
            return returndevice_obj

    def getMountDevices(self):
        """
        """
        return_mount_devices = {}

        for mountdevicekey in self._mountdevices.keys():
            return_mount_devices[mountdevicekey] = self.getMountDevice(mountdevicekey)

        return return_mount_devices

    def addMount(self, key, client, dir_, device, **arguments):
        """
        """
        logfile = options = None
        type_ = "glusterfs"

        if (arguments.has_key("type") and arguments['type']):
            type_ = arguments['type']

        if (arguments.has_key("logfile") and arguments['logfile']):
            logfile = arguments['logfile']

        if (arguments.has_key("options") and arguments['options']):
            options = arguments['options']

        mount_obj = self._mount_tuple(client, dir_, device,
                                      type_, logfile, options)
        self._mounts[key] = mount_obj

    def getMount(self, mountkey):
        """
        """
        return_mount_obj = None
        if not self._mounts.has_key(mountkey):
            return return_mount_obj

        mount_obj = self._mounts[mountkey]
        devicekey = mount_obj.device
        device_obj = self.getMountDevice(devicekey)
        if not device_obj:
            return return_mount_obj
        else:
            return_mount_obj = mount_obj._replace(device=device_obj)

        return return_mount_obj

    def getMounts(self):
        """
        """
        return_mounts = {}

        for mountkey in self._mounts.keys():
            return_mounts[mountkey] = self.getMount(mountkey)

        return return_mounts

    def getMountsKeys(self):
        """
        """
        mounts_keys = []
        mounts_keys.extend(self._mounts.keys())
        return mounts_keys

    def addDefaults(self, **arguments):
        """
        """
        downloadpaths = []

        if (arguments.has_key('downloadpath') and arguments['downloadpath']):
            paths = arguments['downloadpath']
            downloadpaths = [x.strip() for x in paths.split(",")]

        self._gluster_download_paths = downloadpaths

    def setActiveVolume(self, volumekey):
        """
        """
        if self._volumes.has_key(volumekey):
            self._active_volume = volumekey
            return 0
        else:
            return 1

    def getActiveVolume(self):
        """
        """
        active_volume_key = self._active_volume
        active_volume = self.getVolume(active_volume_key)
        return active_volume

    def getGlusterDownloadPaths(self):
        """
        """
        return self._gluster_download_paths

    def getHosts(self):
        """
        """
        all_hosts = {}
        all_hosts.update(self.getServers)
        all_hosts.update(self.getClients)
        return all_hosts

    def getHostsKeys(self):
        """
        """
        hosts_keys = []
        hosts_keys.extend(self._servers.keys())
        hosts_keys.extend(self._clients.keys())
        return hosts_keys

    def getHost(self, hostkey):
        """
        """
        host_obj = None
        host_obj = self.getServer(hostkey)
        if host_obj:
            return host_obj
        else:
            host_obj = self.getClient(hostkey)
            return host_obj

    def addSlave(self, key, hostname, path, **arguments):
        """
        To add slaves for geo-replication defined
        config file
        """
        volumename = None
        if(arguments.has_key('volumename') and arguments['volumename']):
            volumename = arguments['volumename']

        slave_obj = self._slave_tuple(hostname, path, volumename)
        self._slaves[key] = slave_obj

    def getSlave(self, slavekey):
        """
        to getback the slave object for the
        required slave
        """
        return_slave_obj = None
        newhostname = newpath = ''

        if self._slaves.has_key(slavekey):
            slave_obj = self._slaves[slavekey]
        else:
            return return_slave_obj

        hostname_value = slave_obj.hostname
        serverkey = re.split("\.", hostname_value, maxsplit=1)[0]
        server_obj = self.getServer(serverkey)
        if server_obj:
            newhostname = server_obj.hostname
        else:
            return return_brick_obj

        path_value = slave_obj.path
        if re.match("^\/", path_value):
            newpath = path_value
        else:
            exportdir_obj = self.getExportdir(path_value)
            if exportdir_obj:
                newpath = exportdir_obj.dir
            else:
                slave_obj = None
                return return_slave_obj

        return_slave_obj = slave_obj._replace(hostname=newhostname,
                                              path=newpath)

        return return_slave_obj

    def getSlaves(self):
        """
        To get the slave objects of all the slaves defined in
        configuration file
        """
        return_slaves = {}

        for slavekey in self._slaves.keys():
            return_slaves[slavekey] = self.getslave(slavekey)

        return return_slaves

    def getRawSlave(self, slavekey):
        slave_obj = None
        if self._slaves.has_key(slavekey):
            slave_obj = self._slaves[slavekey]
        return slave_obj

    def getSlaveKeys(self):
        """
        """
        slave_keys = []
        slave_keys.extend(self._slaves.keys())
        return slave_keys
