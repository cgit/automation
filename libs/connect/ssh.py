"""
 sshConnection Class for connecting and performing operation on
 remote server using SSH Protocol.
"""
import paramiko
import time
from atfglobals import GlobalObj

class SshConnection():

    def __init__(self):
        self._connection = paramiko.SSHClient()

    def connect(self, host, user, password):
        """
        Objective:
            SSH to Server "host" as User "user"

        Parameter:
            host: Server IP Address
            user: Login Username
            password: Login password

        Return:
            Success: 0
            Failure: 1
        """
        logger = GlobalObj.getLoggerObj()
        self._connection.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        try:
            self._connection.connect(host, username=user, password=password)

        except paramiko.BadHostKeyException as result:
            logger.error("BadHostKeyException: Unable to Connect to Server: '" + host +
                         "' as User: '" + user + "'")
            return 1

        except paramiko.AuthenticationException:
            logger.error("AuthenticationException: Unable to Authenticate " +
                         user + "@" + host)
            return 1

        except paramiko.SSHException:
            logger.error("SSHException: Unknown server " + host)
            return 1

        return 0

    def close(self):
        """
        Objective:
            Close SSH Connections
        """
        self._connection.close()
        return

    def executecommand(self, command, commandInput=None):
        """
        Objective:
            Execute Command "comamnd"

        Parameters:
            command: command to execute

        Return:
            Success: 0
            Failure: 1
        """
        logger = GlobalObj.getLoggerObj()
        output = {}
        output["exitstatus"] = None
        output["stdoutdata"] = None
        output["stderrdata"] = None
        exit_status_ready_flag = True

        try:
            transport = self._connection.get_transport()
            channel = transport.open_session()
            channel.exec_command(command)
            stdin = channel.makefile("wb")
            if commandInput:
                stdin.write(commandInput)

            exit_status = channel.recv_exit_status()
            stdout = channel.makefile("rb")
            stderr = channel.makefile_stderr("rb")

            output["exitstatus"] = exit_status
            output["stdoutdata"] = stdout.readlines()
            output["stderrdata"] = stderr.readlines()

        except paramiko.SSHException:
            logger.error("Unable to Execute Command: " + command)

        return output
