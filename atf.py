"""atf module is the entry point of the regression suite.

CommandLine Usage: python atf.py -f testruninfo.cfg
"""
import argparse
import atfinit
import atfsetup
import atfexecute
import pdb

if __name__ == "__main__":

    argparser = argparse.ArgumentParser(
                prog='atf',
                description="Runs GlusterFS Functional/Regression Test Suite",
                epilog="Report Bugs to dl-qa@gluster.com")

    argparser.add_argument('--atfdir', required=True, type=str,
                           help="Absolute path of directory where automation framework is installed")

    argparser.add_argument('--testruninfo-file', required=True, type=str,
                           help="TestRunInfo FileName")

    argparser.add_argument('--summarylog-file', type=str,
                           default="summarylog.out",
                           help="SummaryLog Filename")

    argparser.add_argument('--summarylog-level', type=str,
                           default='INFO',
                           help="SummaryLog LogLevel")

    argparser.add_argument('--detaillog-file', type=str,
                           default="detaillog.out",
                           help="DetailLog Filename")

    argparser.add_argument('--detaillog-level', type=str,
                           default='DEBUG',
                           help="DetailLog LogLevel")

    argparser.add_argument('--timestamp', type=str,
                           default='yes',
                           help="Attach timestamp to Summary/DetailLog Files: yes|no")

    argparser.add_argument('--stdout-dolog', type=str,
                           default='yes',
                           help="Log to Stdout yes|no")

    argparser.add_argument('--stdoutlog-level', type=str,
                           default='INFO',
                           help="StdoutLog LogLevel")

    args = argparser.parse_args()

    if atfinit.initialize(args):
        exit(1)

    if atfsetup.setup():
        exit(1)

    if atfexecute.execute():
        exit(1)

    else:
        exit(0)

