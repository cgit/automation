#!/usr/bin/env python

import os
import os

exportstr = ''
libdir = os.getcwd() + "/libs/"
for dirname, dirnames, filenames in os.walk(libdir):
    for subdirname in dirnames:
        exportstr = exportstr + os.path.join(dirname, subdirname) + ':'

exportstr = os.getcwd() + ":" + libdir + ":" + exportstr
print exportstr
